package com.getjavajob.training.algo1811.okhanzhin.lesson4;

import java.util.NoSuchElementException;

import static util.Assert.assertEquals;
import static util.Assert.assertBoolean;
import static util.Assert.assertObject;
import static util.Assert.fail;

public class ListIteratorTest {
    private static final String ASSERTION_ERROR = "AssertionError";

    public static void main(String[] args) {
        testListIteratorHasNext();
        testListIteratorNext();
        testListIteratorHasPrevious();
        testListIteratorPrevious();
        testListIteratorNextIndex();
        testListIteratorPreviousIndex();
        testListIteratorRemove();
        testListIteratorSet();
        testListIteratorAdd();
    }

    public static DoublyLinkedList<Integer> initializeList() {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();

        doublyLinkedList.add(0);
        doublyLinkedList.add(1);
        doublyLinkedList.add(2);
        doublyLinkedList.add(3);
        doublyLinkedList.add(4);
        doublyLinkedList.add(5);
        doublyLinkedList.add(6);
        doublyLinkedList.add(7);
        doublyLinkedList.add(8);
        doublyLinkedList.add(9);

        return doublyLinkedList;
    }

    public static void testListIteratorHasNext() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        for (int i = 0; i <= currentList.size() - 1; i++) {
            listIterator.next();
        }

        try {
            listIterator.next();
            fail("NoSuchElementException");
        } catch (NoSuchElementException e) {
            assertEquals("NoSuchElementException", e.getMessage());
        }

        assertBoolean("ListIteratorTest.testListIteratorHasNext", false, listIterator.hasNext());
    }

    public static void testListIteratorNext() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        assertObject("ListIteratorTest.testListIteratorNext", 0, listIterator.next());
    }

    public static void testListIteratorHasPrevious() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        try {
            listIterator.previous();
            fail("NoSuchElementException");
        } catch (NoSuchElementException e) {
            assertEquals("NoSuchElementException", e.getMessage());
        }

        listIterator.next();
        listIterator.next();

        assertBoolean("ListIteratorTest.testListIteratorHasPrevious", true, listIterator.hasPrevious());
    }


    public static void testListIteratorPrevious() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        assertObject("ListIteratorTest.testListIteratorPrevious", 3, listIterator.previous());
    }

    public static void testListIteratorNextIndex() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        listIterator.next();
        listIterator.next();

        assertEquals("ListIteratorTest.testListIteratorNextIndex", 2, listIterator.nextIndex());
    }

    public static void testListIteratorPreviousIndex() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIteratorTest.testListIteratorPreviousIndex", 4, listIterator.previousIndex());
    }

    public static void testListIteratorRemove() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        listIterator.next();
        listIterator.next();

        try {
            listIterator.remove();
            fail(ASSERTION_ERROR);
        } catch (AssertionError e) {
            assertEquals(ASSERTION_ERROR, e.getMessage());
        }

        assertEquals("ListIteratorTest.testListIteratorRemove", new int[]{0, 2, 3, 4, 5, 6, 7, 8, 9}, currentList.toArray());
    }

    public static void testListIteratorSet() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        listIterator.next();
        listIterator.next();

        try {
            listIterator.set(99);
            fail(ASSERTION_ERROR);
        } catch (AssertionError e) {
            assertEquals(ASSERTION_ERROR, e.getMessage());
        }

        assertEquals("ListIteratorTest.testListIteratorSet", new int[]{0, 99, 2, 3, 4, 5, 6, 7, 8, 9}, currentList.toArray());
    }

    public static void testListIteratorAdd() {
        DoublyLinkedList<Integer> currentList = initializeList();
        DoublyLinkedList<Integer>.ListIteratorImpl listIterator = currentList.listIterator();

        listIterator.next();
        listIterator.next();

        try {
            listIterator.add(99);
            fail(ASSERTION_ERROR);
        } catch (AssertionError e) {
            assertEquals(ASSERTION_ERROR, e.getMessage());
        }

        assertEquals("ListIteratorTest.testListIteratorAdd", new int[]{0, 1, 99, 2, 3, 4, 5, 6, 7, 8, 9}, currentList.toArray());
    }
}
