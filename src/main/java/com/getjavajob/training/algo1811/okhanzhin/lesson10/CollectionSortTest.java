package com.getjavajob.training.algo1811.okhanzhin.lesson10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static util.Assert.assertBoolean;
import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class CollectionSortTest {
    public static void main(String[] args) {
        testSort();
        testSize();
        testIsEmpty();
        testAdd();
        testAddAll();
        testRemove();
        testRemoveAll();
        testRetainAll();
        testContains();
        testContainsAll();
        testToArray();
        testClear();
    }

    public static void testSort() {
        String[] stringArray = {"Beta", "Alfa", "Gamma", "Epsilon", "Delta", "Zeta", "Eta", "Theta"};
        List<String> testList = new ArrayList<>(Arrays.asList(stringArray));
        Collections.sort(testList);
        String[] excepted = {"Alfa", "Beta", "Delta", "Epsilon", "Eta", "Gamma", "Theta", "Zeta"};

        assertObject("CollectionSortTest.testSort", new ArrayList<>(Arrays.asList(excepted)), testList);
    }

    public static void testSize() {
        String[] stringArray = {"Beta", "Alfa", "Gamma", "Epsilon", "Delta", "Zeta", "Eta", "Theta"};
        Collection<String> testList = new ArrayList<>(Arrays.asList(stringArray));

        assertEquals("CollectionSortTest.testSize", 8, testList.size());
    }

    public static void testIsEmpty() {
        String[] stringArray = {"Beta", "Alfa", "Gamma", "Epsilon", "Delta", "Zeta", "Eta", "Theta"};
        Collection<String> testList = new ArrayList<>(Arrays.asList(stringArray));

        assertBoolean("CollectionSortTest.testIsEmpty", false, testList.isEmpty());
    }

    public static void testAdd() {
        Collection<Integer> testCollection = new ArrayList<>();

        assertBoolean("CollectionSortTest.testAdd", true, testCollection.add(1));
    }

    public static void testAddAll() {
        Collection<Integer> actualCollection = new ArrayList<>();
        Collection<Integer> exceptedCollection = new ArrayList<>();
        exceptedCollection.add(1);
        exceptedCollection.add(2);
        exceptedCollection.add(3);

        assertBoolean("CollectionSortTest.testAddAllBooleanCheck", true, actualCollection.addAll(exceptedCollection));
        assertObject("CollectionSortTest.testAddAll", exceptedCollection, actualCollection);
    }

    public static void testRemove() {
        Collection<Integer> testCollection = new ArrayList<>();

        assertBoolean("CollectionSortTest.testRemove", true, testCollection.add(1));
    }

    public static void testRemoveAll() {
        String[] stringArray = {"Beta", "Alfa", "Gamma", "Epsilon", "Delta", "Zeta", "Eta", "Theta"};
        Collection<String> testList = new ArrayList<>(Arrays.asList(stringArray));
        testList.removeAll(testList);

        assertObject("CollectionsTest.testRemoveAll", new ArrayList<>(), testList);
    }

    public static void testRetainAll() {
        Collection<Integer> actualCollection = new ArrayList<>();
        Collection<Integer> exceptedCollection = new ArrayList<>();
        actualCollection.add(1);
        actualCollection.add(2);
        exceptedCollection.add(1);

        assertBoolean("CollectionSortTest.testRetainAll", true, actualCollection.retainAll(exceptedCollection));
    }

    public static void testContains() {
        String[] stringArray = {"Beta", "Alfa", "Gamma", "Epsilon", "Delta", "Zeta", "Eta", "Theta"};
        Collection<String> testList = new ArrayList<>(Arrays.asList(stringArray));

        assertBoolean("CollectionSortTest.testContainsTrue", true, testList.contains("Epsilon"));
        assertBoolean("CollectionSortTest.testContainsFalse", false, testList.contains("One"));
    }

    public static void testContainsAll() {
        String[] stringArray = {"Beta", "Alfa", "Gamma", "Epsilon", "Delta", "Zeta", "Eta", "Theta"};
        Collection<String> testList = new ArrayList<>(Arrays.asList(stringArray));

        assertBoolean("CollectionSortTest.testContainsAllTrue", true, testList.containsAll(Arrays.asList(stringArray)));
        testList.remove("Eta");
        assertBoolean("CollectionSortTest.testContainsAllFalse", false, testList.containsAll(Arrays.asList(stringArray)));
    }

    private static void testToArray() {
        Collection<Integer> testCollection = new ArrayList<>();
        testCollection.add(1);

        assertEquals("CollectionSortTest.testToArray", new int[]{1}, testCollection.toArray());
    }

    private static void testClear() {
        Collection<Integer> testCollection = new ArrayList<>();
        testCollection.add(1);
        testCollection.add(2);
        testCollection.add(3);
        testCollection.clear();

        assertObject("CollectionSortTest.testClear", new ArrayList<>(), testCollection);
    }
}
