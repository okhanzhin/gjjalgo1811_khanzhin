package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import java.util.Comparator;
import com.getjavajob.training.algo1811.okhanzhin.lesson7.binary.LinkedBinaryTree.NodeImpl;

import static util.Assert.assertObject;

public class RedBlackTreeTest {
    public static void main(String[] args) {
        testAdd();
        testRemoveRoot();
        testRemoveRedNote();
        testRemoveBlackNote();
    }

    public static void testAdd() {
        RedBlackTree<Integer> tree = new RedBlackTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        tree.addRoot(20);
        tree.add(tree.root(), 41);
        tree.add(tree.root(), 12);
        tree.add(tree.root(), 5);
        tree.add(tree.root(), 13);
        tree.add(tree.root(), 16);
        tree.add(tree.root(), 50);
        tree.add(tree.root(), 47);
        tree.add(tree.root(), 41);
        tree.add(tree.root(), 45);
        tree.add(tree.root(), 52);
        tree.add(tree.root(), 49);
        tree.add(tree.root(), 60);
        tree.add(tree.root(), 51);

        assertObject("RedBlackTreeTest.testAdd", "(20 - B(12 - B(5 - B, 13 - B, 16 - R)), 47 - B(41 - B, 45 - R), 50 - R(49 - B, 52 - B(51 - R, 60 - R)))))", tree.toString());
    }

    public static void testRemoveRoot() {
        RedBlackTree<Integer> tree = new RedBlackTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        tree.addRoot(20);
        tree.add(tree.root(), 41);
        tree.add(tree.root(), 32);
        tree.add(tree.root(), 12);
        tree.add(tree.root(), 5);
        tree.add(tree.root(), 13);
        tree.add(tree.root(), 16);
        tree.add(tree.root(), 50);
        tree.add(tree.root(), 47);
        tree.add(tree.root(), 42);

        tree.remove(tree.root());

        assertObject("RedBlackTreeTest.testRemoveRoot", "(20 - B(12 - B(5 - B, 13 - R), 32 - B, 47 - R(41 - B, 42 - R), 50 - B))))", tree.toString());
    }

    public static void testRemoveRedNote() {
        RedBlackTree<Integer> tree = new RedBlackTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        tree.addRoot(20);
        tree.add(tree.root(), 41);
        tree.add(tree.root(), 32);
        tree.add(tree.root(), 12);
        tree.add(tree.root(), 5);
        tree.add(tree.root(), 13);
        tree.add(tree.root(), 16);
        tree.add(tree.root(), 50);
        tree.add(tree.root(), 47);
        tree.add(tree.root(), 42);

        NodeImpl<Integer> redNodeToDelete = ((NodeImpl<Integer>) tree.root()).getRightChild().getLeftChild();

        tree.remove(redNodeToDelete);

        assertObject("RedBlackTreeTest.testRemoveRedNode", "(16 - B(12 - B(5 - B, 13 - R), 32 - B, 47 - R(41 - B, 42 - R), 50 - B))))", tree.toString());
    }

    public static void testRemoveBlackNote() {
        RedBlackTree<Integer> tree = new RedBlackTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        tree.addRoot(20);
        tree.add(tree.root(), 41);
        tree.add(tree.root(), 32);
        tree.add(tree.root(), 12);
        tree.add(tree.root(), 5);
        tree.add(tree.root(), 13);
        tree.add(tree.root(), 16);
        tree.add(tree.root(), 50);
        tree.add(tree.root(), 47);
        tree.add(tree.root(), 42);

        NodeImpl<Integer> blackNodeToDelete = ((NodeImpl<Integer>) tree.root()).getLeftChild();

        tree.remove(blackNodeToDelete);

        assertObject("RedBlackTreeTest.testRemoveBlackNode", "(16 - B(5 - B, 13 - R), 32 - B(20 - R, 47 - R(41 - B, 42 - R), 50 - B))))", tree.toString());
    }
}
