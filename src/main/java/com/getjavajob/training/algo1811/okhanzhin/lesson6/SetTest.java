package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.HashSet;
import java.util.Set;

import static util.Assert.assertBoolean;
import static util.Assert.assertObject;

public class SetTest {
    public static void main(String[] args) {
        testAdd();
        testAddAll();
    }

    public static void testAdd() {
        Set<Integer> testSet = new HashSet<>();

        assertBoolean("SetTest.testAdd", true, testSet.add(100));
    }

    public static void testAddAll() {
        Set<Integer> firstSet = new HashSet<>();

        firstSet.add(10);
        firstSet.add(20);
        firstSet.add(30);

        Set<Integer> secondSet = new HashSet<>();

        secondSet.addAll(firstSet);

        assertObject("SetTest.testAddAll", firstSet.toString(), secondSet.toString());
    }
}
