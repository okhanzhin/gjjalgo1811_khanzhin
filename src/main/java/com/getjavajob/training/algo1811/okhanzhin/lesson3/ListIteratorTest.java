package com.getjavajob.training.algo1811.okhanzhin.lesson3;

import java.util.NoSuchElementException;
import static util.Assert.assertEquals;
import static util.Assert.assertBoolean;
import static util.Assert.assertObject;
import static util.Assert.fail;

public class ListIteratorTest {
    private static final String ASSERTION_ERROR = "AssertionError";

    public static void main(String[] args) {
        testListIteratorHasNext();
        testListIteratorNext();
        testListIteratorHasPrevious();
        testListIteratorPrevious();
        testListIteratorNextIndex();
        testListIteratorPreviousIndex();
        testListIteratorRemove();
        testListIteratorSet();
        testListIteratorAdd();
    }

    public static DynamicArray<Integer> initializeDynamicArray() {
        DynamicArray <Integer> dynamicArray = new DynamicArray<>();

        dynamicArray.add(1);
        dynamicArray.add(2);
        dynamicArray.add(3);
        dynamicArray.add(4);
        dynamicArray.add(5);

        return dynamicArray;
    }

    public static void testListIteratorHasNext() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        try {
            listIterator.next();
            fail("No Next Element!");
        } catch (NoSuchElementException e) {
            assertEquals("No Next Element!", e.getMessage());
        }

        assertBoolean("ListIteratorTest.testListIteratorHasNext", false, listIterator.hasNext());
    }

    public static void testListIteratorNext() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        assertObject("ListIteratorTest.testIteratorNext", 1, listIterator.next());
    }

    public static void testListIteratorHasPrevious() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();
        listIterator.next();

        assertBoolean("ListIteratorTest.testListIteratorHasPrevious", true, listIterator.hasPrevious());
    }

    public static void testListIteratorPrevious() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();

        assertObject("ListIteratorTest.testListIteratorPrevious", 1, listIterator.previous());
    }

    public static void testListIteratorNextIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();
        listIterator.next();

        assertEquals("ListIteratorTest.testListIteratorNextIndex", 2, listIterator.nextIndex());
    }

    public static void testListIteratorPreviousIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIteratorTest.testListIteratorPreviousIndex", 4, listIterator.previousIndex());
    }

    public static void testListIteratorRemove() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();
        listIterator.next();

        try {
            listIterator.remove();
            fail(ASSERTION_ERROR);
        } catch (AssertionError e) {
            assertEquals(ASSERTION_ERROR, e.getMessage());
        }

        assertEquals("ListIteratorTest.testListIteratorRemove", new int[] {1, 2, 4, 5}, array.toArray());
    }

    public static void testListIteratorSet() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();
        listIterator.next();

        try {
            listIterator.set(100);
            fail(ASSERTION_ERROR);
        } catch (AssertionError e) {
            assertEquals(ASSERTION_ERROR, e.getMessage());
        }

        assertEquals("ListIteratorTest.testListIteratorSet", new int[] {1, 2, 100, 4, 5}, array.toArray());
    }

    public static void testListIteratorAdd() {
        DynamicArray<Integer> array = initializeDynamicArray();
        DynamicArray<Integer>.ListIteratorImpl listIterator = array.listIterator();

        listIterator.next();

        try {
            listIterator.add(100);
            fail(ASSERTION_ERROR);
        } catch (AssertionError e) {
            assertEquals(ASSERTION_ERROR, e.getMessage());
        }

        assertEquals("ListIteratorTest.testListIteratorAdd", new int[] {1, 100, 2, 3, 4, 5}, array.toArray());
    }
}
