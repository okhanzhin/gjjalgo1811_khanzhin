package com.getjavajob.training.algo1811.okhanzhin.lesson10;

import static com.getjavajob.training.algo1811.okhanzhin.lesson10.BubbleSort.sort;
import static util.Assert.assertEquals;

public class BubbleSortTest {
    public static void main(String[] args) {
        testSort();
        testAlreadySortValues();
    }

    public static void testSort() {
        int[] array = new int[]{1, 6, 2, 90, 7, 8, 0};
        int[] exceptedArray = new int[]{0, 1, 2, 6, 7, 8, 90};

        assertEquals("BubbleSortTest.testSort", exceptedArray, sort(array));
    }

    public static void testAlreadySortValues() {
        int[] array = new int[]{0, 1, 2, 6, 7, 8, 90};
        int[] exceptedArray = new int[]{0, 1, 2, 6, 7, 8, 90};

        assertEquals("BubbleSortTest.testAlreadySortValues", exceptedArray, sort(array));
    }
}
