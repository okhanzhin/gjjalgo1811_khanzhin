package com.getjavajob.training.algo1811.okhanzhin.lesson4;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;
import static util.Assert.assertBoolean;
import static util.Assert.fail;

public class DoublyLinkedListTest {
    private static final String INDEX_OUT_DESCRIPTION = "IndexOutOfBoundsException";
    private static final String FAIL_DESCRIPTION = "Exception Test Failed";
    private static final String NULL_POINTER_DESCRIPTION = "NullPointerException";

    public static void main(String[] args) {
        testDefaultEmptyConstructor();
        testDefaultInitialConstructor();

        testBooleanAdd();
        testVoidAddFirst();
        testVoidAddMiddle();
        testVoidAddLast();

        testVoidAddBeyondTheLowerIndex();
        testVoidAddAboveTheUpperIndex();

        testBooleanRemoveTrue();
        testBooleanRemoveFalse();

        testInitialRemoveFirst();
        testInitialRemoveMiddle();
        testInitialRemoveLast();

        testInitialRemoveBeyondTheLowerIndex();
        testInitialRemoveAboveTheUpperIndex();

        testGet();
        testGetBeyondTheLowerIndex();
        testGetAboveTheUpperIndex();
    }

    public static DoublyLinkedList<Integer> initializeList() {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();

        doublyLinkedList.add(0);
        doublyLinkedList.add(1);
        doublyLinkedList.add(2);
        doublyLinkedList.add(3);
        doublyLinkedList.add(4);
        doublyLinkedList.add(5);
        doublyLinkedList.add(6);
        doublyLinkedList.add(7);
        doublyLinkedList.add(8);

        return doublyLinkedList;
    }

    public static void testDefaultEmptyConstructor() {
        DoublyLinkedList<Integer> defaultEmpty = new DoublyLinkedList<>();

        assertEquals("DoublyLinkedListTest.testDefaultEmptyConstructor", 0, defaultEmpty.size());
    }

    public static void testDefaultInitialConstructor() {
        DoublyLinkedList<Integer> defaultInitial = new DoublyLinkedList<>(5);

        assertEquals("DoublyLinkedListTest.testDefaultInitialConstructor", 5, defaultInitial.size());
    }

    public static void testBooleanAdd() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        assertBoolean("DoublyLinkedListTest.testBooleanAdd", true, doublyList.add(10));
    }

    public static void testVoidAddFirst() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.add(0, 99);
            assertEquals("DoublyLinkedListTest.testVoidAddFirst", new int[]{99, 0, 1, 2, 3, 4, 5, 6, 7, 8}, doublyList.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testVoidAddMiddle() {
        DoublyLinkedList<Integer> doublyList = initializeList();
        try {
            doublyList.add(4, 99);
            assertEquals("DoublyLinkedListTest.testVoidAddMiddle", new int[]{0, 1, 2, 3, 99, 4, 5, 6, 7, 8}, doublyList.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testVoidAddLast() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.add(9, 99);
            assertEquals("DoublyLinkedListTest.testVoidAddLast", new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 99}, doublyList.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testVoidAddBeyondTheLowerIndex() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.add(-1, 99);
        } catch (IndexOutOfBoundsException e) {
            assertObject("DoublyLinkedListTest.testVoidAddBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testVoidAddAboveTheUpperIndex() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.add(10, 99);
        } catch (IndexOutOfBoundsException e) {
            assertObject("DoublyLinkedListTest.testVoidAddAboveTheUpperIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testBooleanRemoveTrue() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        assertBoolean("DoublyLinkedListTest.testBooleanRemoveFirst", true, doublyList.remove(new Integer(0)));
    }

    public static void testBooleanRemoveFalse() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        assertBoolean("DoublyLinkedListTest.testBooleanRemoveFirst", false, doublyList.remove(new Integer(10)));
    }

    public static void testInitialRemoveFirst() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.remove(0);
            assertEquals("DoublyLinkedListTest.testInitialRemoveFirst", new int[]{1, 2, 3, 4, 5, 6, 7, 8}, doublyList.toArray());

            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testInitialRemoveMiddle() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.remove(4);
            assertEquals("DoublyLinkedListTest.testInitialRemoveMiddle", new int[]{0, 1, 2, 3, 5, 6, 7, 8}, doublyList.toArray());

            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testInitialRemoveLast() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.remove(8);
            assertEquals("DoublyLinkedListTest.testInitialRemoveLast", new int[]{0, 1, 2, 3, 4, 5, 6, 7}, doublyList.toArray());

            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testInitialRemoveBeyondTheLowerIndex() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.remove(-1);
        } catch (IndexOutOfBoundsException e1) {
            assertObject("DoublyLinkedListTest.testInitialRemoveBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e1.getMessage());
        } catch (NullPointerException e2) {
            assertObject("DoublyLinkedListTest.testInitialRemoveBeyondTheLowerIndex", NULL_POINTER_DESCRIPTION, e2.getMessage());
        }
    }

    public static void testInitialRemoveAboveTheUpperIndex() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.remove(10);
        } catch (IndexOutOfBoundsException e1) {
            assertObject("DoublyLinkedListTest.testInitialRemoveAboveTheUpperIndex", INDEX_OUT_DESCRIPTION, e1.getMessage());
        } catch (NullPointerException e2) {
            assertObject("DoublyLinkedListTest.testInitialRemoveAboveTheUpperIndex", NULL_POINTER_DESCRIPTION, e2.getMessage());
        }
    }

    public static void testGet() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            assertEquals("DoublyLinkedListTest.testGet", 5, doublyList.get(5));

            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testGetBeyondTheLowerIndex() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.get(-1);

        } catch (IndexOutOfBoundsException e) {
            assertObject("DoublyLinkedListTest.testGetBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testGetAboveTheUpperIndex() {
        DoublyLinkedList<Integer> doublyList = initializeList();

        try {
            doublyList.get(9);

        } catch (IndexOutOfBoundsException e) {
            assertObject("DoublyLinkedListTest.testGetAboveTheUpperIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }
}
