package com.getjavajob.training.algo1811.okhanzhin.lesson10;

import util.StopWatch;
import java.util.Random;

import static com.getjavajob.training.algo1811.okhanzhin.lesson10.QuickSort.sort;
import static util.Assert.assertEquals;

public class QuickSortTest {
    private static final int INTEGER_MAX_VALUE = Integer.MAX_VALUE;

    public static void main(String[] args) {
        testSort();
        testOverFlowSort();
    }

    public static void testSort() {
        int[] actualArray = {1, 6, 2, 90, 7, 8, 0, 16, 14, 87, 77, 105};
        int[] exceptedArray = new int[]{0, 1, 2, 6, 7, 8, 14, 16, 77, 87, 90, 105};

        sort(actualArray);

        assertEquals("QuickSortTest.testSort", exceptedArray, actualArray);
    }

    public static void testOverFlowSort() {
        int[] testArray = new int[INTEGER_MAX_VALUE / 3]; // maximum possible size

        Random random = new Random();

        for (int i = 0; i <= testArray.length - 1; i++) {
            testArray[i] = random.nextInt();
        }

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        sort(testArray);
        long quickSortTime = stopWatch.getElapsedTime();
        System.out.println("QuickSort time for INTEGER_MAX_VALUE/3 array size: " + quickSortTime + "ms.");
    }
}
