package com.getjavajob.training.algo1811.okhanzhin.lesson4;

import java.util.Iterator;
import java.util.List;

import static com.getjavajob.training.algo1811.okhanzhin.lesson4.CollectionTest.initArrayList;
import static util.Assert.assertEquals;

public class ListTest {
    public static void main(String[] args) {
        testIterator();
        testIndexIterator();
        testGet();
        testSet();
        testIndexAdd();
        testIndexRemove();
        testIndexOf();
        testLastIndexOf();
    }

    public static void testIterator() {
        List<Integer> currentList = initArrayList();
        Iterator<Integer> iterator = currentList.iterator();

        assertEquals("ListTest.testIterator", 1, iterator.next());
    }

    public static void testIndexIterator() {
        List<Integer> currentList = initArrayList();
        Iterator<Integer> listIterator = currentList.listIterator(2);

        assertEquals("ListTest.testIndexIterator", 3, listIterator.next());
    }

    public static void testGet() {
        List<Integer> currentList = initArrayList();

        assertEquals("ListTest.testGet", 5, currentList.get(4));
    }

    public static void testSet() {
        List<Integer> currentList = initArrayList();
        currentList.set(0, 99);

        assertEquals("ListTest.testGet", 99, currentList.get(0));
    }

    public static void testIndexAdd() {
        List<Integer> currentList = initArrayList();

        currentList.add(0, 99);
        currentList.add(4,99);
        currentList.add(currentList.size(), 99);

        assertEquals("ListTest.testIndexAdd", new int[] {99, 1, 2, 3, 99, 4, 5, 99}, currentList.toArray());
    }

    public static void testIndexRemove() {
        List<Integer> currentList = initArrayList();

        currentList.remove(0);
        currentList.remove(1);
        currentList.remove(currentList.size() - 1);

        assertEquals("ListTest.testIndexAdd", new int[] {2, 4}, currentList.toArray());
    }

    public static void testIndexOf() {
        List<Integer> currentList = initArrayList();

        assertEquals("ListTest.testIndexOf", 3, currentList.indexOf(4));
    }

    public static void testLastIndexOf() {
        List<Integer> currentList = initArrayList();
        currentList.add(0, 66);
        currentList.add(3, 66);
        currentList.add(currentList.size(), 66);

        assertEquals("ListTest.testLastIndexOf", currentList.size() - 1, currentList.lastIndexOf(66));
    }
}
