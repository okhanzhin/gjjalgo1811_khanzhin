package com.getjavajob.training.algo1811.okhanzhin.lesson1;

import java.util.Arrays;

public class Task7 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(swapVarsBitwiseCaseOne(15, 25)));
        System.out.println(Arrays.toString(swapVarsBitwiseCaseTwo(15, 25)));

        System.out.println(Arrays.toString(swapVarsArithmeticCaseOne(15, 25)));
        System.out.println(Arrays.toString(swapVarsArithmeticCaseTwo(15, 25)));
    }

    public static int[] swapVarsBitwiseCaseOne(int numberOne, int numberTwo) {
        int var1 = numberOne;
        int var2 = numberTwo;

        var1 = var1 ^ var2;
        var2 = var2 ^ var1;
        var1 = var1 ^ var2;

        return new int[]{var1, var2};
    }

    public static int[] swapVarsBitwiseCaseTwo(int numberOne, int numberTwo) {
        int var1 = numberOne;
        int var2 = numberTwo;

        var1 = ~var1 & var2 | ~var2 & var1;
        var2 = ~var1 & var2 | ~var2 & var1;
        var1 = ~var1 & var2 | ~var2 & var1;

        return new int[]{var1, var2};
    }

    public static int[] swapVarsArithmeticCaseOne(int numberOne, int numberTwo) {
        int var1 = numberOne;
        int var2 = numberTwo;

        var1 += var2;
        var2 = var1 - var2;
        var1 -= var2;

        return new int[]{var1, var2};
    }

    public static int[] swapVarsArithmeticCaseTwo(int numberOne, int numberTwo) {
        int var1 = numberOne;
        int var2 = numberTwo;

        var1 *= var2;
        var2 = var1 / var2;
        var1 /= var2;

        return new int[]{var1, var2};
    }
}
