package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import static util.Assert.assertBoolean;
import static util.Assert.assertEquals;
import static util.Assert.assertObject;

import java.util.HashMap;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testContainsKey();
        testContainsValue();
        testPut();
        testPutAll();
        testGet();
        testRemove();
        testClear();
        testKeySet();
        testEntrySet();
        testCollectionValues();
    }

    public static void testSize() {
        Map<Integer, Integer> testMap = new HashMap<>();

        testMap.put(1, 1);
        testMap.put(2, 2);

        assertEquals("MapTest.testSize", 2, testMap.size());

    }

    public static void testIsEmpty() {
        Map<Integer, Integer> testMap = new HashMap<>();

        testMap.put(1, 1);

        assertBoolean("MapTest.testIsEmpty", false, testMap.isEmpty());
    }

    public static void testContainsKey() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertBoolean("MapTest.testContainsKey", true, testMap.containsKey(1));
    }

    private static void testContainsValue() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertBoolean("MapTest.testContainsKey", true, testMap.containsValue("Three"));
    }

    public static void testPut() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("MapTest.testPut","{1=One, 2=Two, 3=Three}", testMap.toString());
    }

    public static void testPutAll() {
        Map<Integer, String> firstMap = new HashMap<>();
        Map<Integer, String> secondMap = new HashMap<>();

        firstMap.put(1, "One");
        firstMap.put(2, "Two");
        firstMap.put(3, "Three");

        secondMap.putAll(firstMap);

        assertObject("MapTest.testPutAll","{1=One, 2=Two, 3=Three}", secondMap.toString());
    }

    public static void testGet() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("MapTest.testGet", "Two", testMap.get(2));
    }

    public static void testRemove() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("MapTest.testRemove", "Three", testMap.remove(3));
    }

    public static void testClear() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        testMap.clear();

        assertBoolean("MapTest.testClear", true, testMap.isEmpty());
    }

    public static void testKeySet() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertEquals("MapTest.testKeySet", new int[] {1, 2, 3}, testMap.keySet().toArray());
    }

    public static void testEntrySet() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("MapTest.testEntrySet", "[1=One, 2=Two, 3=Three]", testMap.entrySet().toString());
    }

    public static void testCollectionValues() {
        Map<Integer, String> testMap = new HashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("MapTest.testCollectionValues", "[One, Two, Three]", testMap.values().toString());
    }
}
