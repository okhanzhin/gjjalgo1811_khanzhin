package com.getjavajob.training.algo1811.okhanzhin.lesson10;

public class InsertionSort {
    public static int[] sort(int[] array) {
        for (int outerCount = 1; outerCount < array.length; outerCount++) {
            int temp = array[outerCount];
            int innerCount = outerCount;
            while (innerCount > 0 && array[innerCount - 1] >= temp) {
                array[innerCount] = array[innerCount - 1];
                --innerCount;
            }
            array[innerCount] = temp;
        }
        return array;
    }
}
