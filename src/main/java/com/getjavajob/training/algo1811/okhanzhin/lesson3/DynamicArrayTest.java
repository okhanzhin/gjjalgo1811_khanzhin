package com.getjavajob.training.algo1811.okhanzhin.lesson3;

import static util.Assert.assertEquals;
import static util.Assert.assertBoolean;
import static util.Assert.assertObject;
import static util.Assert.fail;

public class DynamicArrayTest {
    private static final String INDEX_OUT_DESCRIPTION = "ArrayIndexOutOfBoundsException";
    private static final String FAIL_DESCRIPTION = "Exception Test Failed";

    public static void main(String[] args) {
        testDefaultCapacityConstructor();
        testInitialCapacityConstructor();

        testBooleanAdd();
        testVoidAddFirst();
        testVoidAddMiddle();
        testVoidAddLast();

        testVoidAddBeyondTheLowerIndex();
        testVoidAddAboveTheUpperIndex();

        testSet();
        testSetBeyondTheLowerIndex();
        testSetAboveTheUpperIndex();

        testGet();
        testGetBeyondTheLowerIndex();
        testGetAboveTheUpperIndex();

        testObjectRemoveFirst();
        testObjectRemoveMiddle();
        testObjectRemoveLast();

        testObjectRemoveBeyondTheLowerIndex();
        testObjectRemoveAboveTheLowerIndex();

        testBooleanRemove();

        testSize();

        testIndexOf();
        testIndexOfElementOutOfRange();

        testContains();
        testNotContains();

        testToArray();
    }

    public static DynamicArray<Integer> initializeDynamicArray() {
        DynamicArray<Integer> dynamicArray = new DynamicArray<>(8);

        dynamicArray.add(10);
        dynamicArray.add(20);
        dynamicArray.add(30);
        dynamicArray.add(40);
        dynamicArray.add(50);
        dynamicArray.add(60);
        dynamicArray.add(70);
        dynamicArray.add(80);

        return dynamicArray;
    }

    public static void testDefaultCapacityConstructor() {
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        assertEquals("DynamicArray.testDefaultCapacityConstructor", 0, dynamicArray.size());
    }

    public static void testInitialCapacityConstructor() {
        DynamicArray<Integer> dynamicArray = new DynamicArray<>(5);

        assertEquals("DynamicArray.testInitialCapacityConstructor", 0, dynamicArray.size());
    }

    public static void testBooleanAdd() {
        initializeDynamicArray();

        assertBoolean("DynamicArray.testBooleanAdd", true, initializeDynamicArray().add(100));
    }

    public static void testVoidAddFirst() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.add(0, 99);
            assertEquals("DynamicArray.testVoidAddFirst", new int[]{99, 10, 20, 30, 40, 50, 60, 70, 80}, array.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }

    }

    public static void testVoidAddMiddle() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.add(5, 99);
            assertEquals("DynamicArray.testVoidAddMiddle", new int[]{10, 20, 30, 40, 50, 99, 60, 70, 80}, array.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }

    }

    public static void testVoidAddLast() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.add(7, 99);
            assertEquals("DynamicArray.testVoidAddLast", new int[]{10, 20, 30, 40, 50, 60, 70, 99, 80}, array.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }


    }

        public static void testVoidAddBeyondTheLowerIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.add(-1, 99);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testVoidAddBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testVoidAddAboveTheUpperIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.add(10, 99);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testVoidAddAboveTheUpperIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testSet() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.set(5, 99);
            assertEquals("DynamicArray.testSet", new int[]{10, 20, 30, 40, 50, 99, 70, 80}, array.toArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testSetBeyondTheLowerIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.set(-1, 99);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testVoidSetBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testSetAboveTheUpperIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.set(10, 99);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testVoidSetAboveTheUpperIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testGet() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            assertEquals("DynamicArray.testGet", 60, array.get(5));
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testGetBeyondTheLowerIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.get(-1);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testGetBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testGetAboveTheUpperIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.get(10);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testGetAboveTheUpperIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testObjectRemoveFirst() {
        DynamicArray<Integer> array = initializeDynamicArray();
        Integer expected = array.get(0);

        try {
            assertEquals("DynamicArray.testObjectRemoveFirst", expected, array.remove(0));
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testObjectRemoveMiddle() {
        DynamicArray<Integer> array = initializeDynamicArray();
        Integer expected = array.get(5);

        try {
            assertEquals("DynamicArray.testObjectRemoveFirst", expected, array.remove(5));
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testObjectRemoveLast() {
        DynamicArray<Integer> array = initializeDynamicArray();
        Integer expected = array.get(7);

        try {
            assertEquals("DynamicArray.testObjectRemoveFirst", expected, array.remove(7));
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testObjectRemoveBeyondTheLowerIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.remove(-1);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testObjectRemoveBeyondTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testObjectRemoveAboveTheLowerIndex() {
        DynamicArray<Integer> array = initializeDynamicArray();

        try {
            array.remove(10);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertObject("DynamicArray.testObjectRemoveAboveTheLowerIndex", INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testBooleanRemove() {
        DynamicArray<Integer> array = initializeDynamicArray();
        assertBoolean("DynamicArray.testBooleanRemove", true, array.remove(array.get(5)));
    }

    public static void testSize() {
        DynamicArray<Integer> array = initializeDynamicArray();
        assertEquals("DynamicArray.testSize", 8, array.size());
    }

    public static void testIndexOf() {
        DynamicArray<Integer> array = initializeDynamicArray();
        assertEquals("DynamicArray.testIndexOf", 0, array.indexOf(array.get(0)));
    }

    public static void testIndexOfElementOutOfRange() {
        DynamicArray<Integer> array = initializeDynamicArray();
        try {
            assertEquals("DynamicArray.testIndexOf", -1, array.indexOf(array.get(10)));
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals(INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testContains() {
        DynamicArray<Integer> array = initializeDynamicArray();
        assertBoolean("DynamicArray.testContains", true, array.contains(array.get(5)));
    }

    public static void testNotContains() {
        DynamicArray<Integer> array = initializeDynamicArray();
        try {
            assertBoolean("DynamicArray.testContains", true, array.contains(array.get(10)));
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals(INDEX_OUT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testToArray() {
        DynamicArray<Integer> array = initializeDynamicArray();
        assertEquals("DynamicArray.testToArray", new int[]{10, 20, 30, 40, 50, 60, 70, 80}, array.toArray());
    }
}