package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.Objects;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class AssociativeArray<K, V> {
    private static final int DEFAULT_CAPACITY = 1 << 4;
    private static final float LOAD_FACTOR = 0.75f;
    private static final int MAXIMUM_CAPACITY = 1 << 30;
    private static final int NULL_KEY_HASH = 0;

    private Element<K, V>[] table = new Element[DEFAULT_CAPACITY];
    private int threshold = (int) (DEFAULT_CAPACITY * LOAD_FACTOR);
    private int size;


    private int getHash(Object key) {
        if (key == null) {
            return NULL_KEY_HASH;
        }
        int h = 17;
        return 31 * h + key.hashCode();
    }

    private int indexFor(int hash, int length) {
        return abs(hash % length);
    }

    public V get(K key) {
        Element<K, V> getBuffer = getElement(key);
        return getBuffer == null ? null : getBuffer.getValue();
    }

    private V getNullElementValue(K key) {
        if (size != 0) {
            for (Element<K, V> cursor = this.table[0]; cursor != null; cursor = cursor.next) {
                if (cursor.getKey() == null) {
                    return cursor.getValue();
                }
            }
        }

        return null;
    }

    private Element<K, V> getElement(K key) {
        if (size == 0) {
            return null;
        } else {
            int hash = getHash(key);
            for (Element<K, V> cursor = table[indexFor(hash, table.length)]; cursor != null; cursor = cursor.next) {
                if (cursor.getHash() == hash && (cursor.getKey() == key || (key != null && key.equals(cursor.getKey())))) {
                    return cursor;
                }
            }
        }
        return null;
    }

    public V remove(K key) {
        Element<K, V> element = removeElement(key);

        if (element != null) {
            return element.getValue();
        }

        return null;
    }

    private Element<K, V> removeElement(K key) {
        int hash = getHash(key);
        int i = indexFor(hash, table.length);
        Element<K, V> removedElement = null;
        Element<K, V> prev = table[i];

        for (Element<K, V> cursor = prev; cursor != null; cursor = cursor.next) {
            Element<K, V> next = cursor.next;
            if (cursor.getHash() == hash && (cursor.getKey() == key || (key != null && key.equals(cursor.getKey())))) {
                removedElement = cursor;
                if (prev == cursor) {
                    table[i] = next;
                } else {
                    prev.setNext(next);
                }
                --size;
                return cursor;
            }
            prev = cursor;
        }

        return null;
    }

    public V put(K key, V value) {
        if (key == null) {
            return putNullKey(value);
        }

        int hash = getHash(key);
        int index = indexFor(hash, table.length);

        for (Element<K, V> cursor = table[index]; cursor != null; cursor = cursor.next) {
            K k;
            if (cursor.hash == hash && ((k = cursor.getKey()) == key || key.equals(k))) {
                V oldValue = cursor.getValue();
                cursor.setValue(value);
                return oldValue;
            }
        }

        putElement(hash, key, value, index);
        return null;
    }

    private V putNullKey(V value) {
        for (Element<K, V> cursor = this.table[0]; cursor != null; cursor = cursor.next) {
            if (cursor.getKey() == null) {
                V oldValue = cursor.getValue();
                cursor.setValue(value);
                return oldValue;
            }
        }

        putElement(NULL_KEY_HASH, null, value, 0);
        return null;
    }

    private void putElement(int hash, K key, V value, int bucketIndex) {
        if ((size >= threshold) && (table[bucketIndex] != null)) {
            resizeArray(2 * table.length);
            hash = (key != null) ? getHash(key) : 0;
            bucketIndex = indexFor(hash, table.length);
        }

        createElement(hash, key, value, bucketIndex);
    }

    private void createElement(int hash, K key, V value, int bucketIndex) {
        Element<K, V> element = table[bucketIndex];
        table[bucketIndex] = new Element<K, V>(hash, key, value, element);
        size++;
    }

    private void resizeArray(int newCapacity) {
        Element<K, V>[] oldTable = table;
        int oldCapacity = table.length;
        if (oldCapacity >= MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }

        Element<K, V>[] newTable = new Element[newCapacity];
        transfer(newTable);
        table = newTable;
        threshold = (int) min(newCapacity * LOAD_FACTOR, MAXIMUM_CAPACITY + 1);
    }

    private void transfer(Element<K, V>[] newTable) {
        int newCapacity = newTable.length;
        for (Element<K, V> cursor : table) {
            while (cursor != null) {
                Element<K, V> next = cursor.next;
                int index = indexFor(cursor.hashCode(), newCapacity);
                cursor.setNext(newTable[index]);
                newTable[index] = cursor;
                cursor = next;
            }
        }
    }

    public void displayTable() {
        for (int i = 0; i < size; i++) {
            String displayBucket = getDisplayBucket(i);
            if (displayBucket != null) {
                System.out.print(i + ".");
                System.out.println(displayBucket);
            }
        }
    }

    private String getDisplayBucket(int i) {
        StringBuilder sb = new StringBuilder(" Bucket (first >> last): ");
        boolean find = false;
        for (Element<K, V> cursor = table[i]; cursor != null; cursor = cursor.next) {
            sb.append("[").append(cursor.getKey().toString()).append("-").append(cursor.getValue().toString()).append("]");
            find = true;
        }

        return find ? sb.toString() : null;
    }

    private static class Element<K, V> {
        final K key;
        final int hash;
        V value;
        Element<K, V> next;

        protected Element(int hash, K key, V value, Element<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public int getHash() {
            return hash;
        }

        public void setNext(Element<K, V> next) {
            this.next = next;
        }

        @Override
        public String toString() {
            return "key=" + key + ", value=" + value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || o.getClass() != this.getClass()) {
                Element<?, ?> element = (Element<?, ?>) o;
                return Objects.equals(key, element.getKey()) && Objects.equals(value, element.getValue()) || Objects.equals(hash, element.hashCode());
            }
            return false;
        }

        @Override
        public int hashCode() {
            int h = 17;
            h = 31 * h + key.hashCode();
            h = 31 * h + value.hashCode();

            return h;
        }
    }
}
