package com.getjavajob.training.algo1811.okhanzhin.lesson8.balanced;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.Node;
import com.getjavajob.training.algo1811.okhanzhin.lesson8.BinarySearchTree;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo1811.okhanzhin.lesson7.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (parent == root()) {
            child.setParent(null);
            addRoot(child);
        } else {
            if (parent.getParent().getLeftChild() == parent) {
                parent.getParent().setLeftChild(child);
            } else {
                parent.getParent().setRightChild(child);
            }
            child.setParent(parent.getParent());
        }

        if (makeLeftChild) {
            rotateRight(parent, child);
        } else {
            rotateLeft(parent, child);
        }
    }

    private void rotateLeft(NodeImpl<E> parent, NodeImpl<E> child) {
        parent.setParent(child);
        if (child.getLeftChild() != null) {
            child.getLeftChild().setParent(parent);
        }
        parent.setRightChild(child.getLeftChild());
        child.setLeftChild(parent);
    }

    private void rotateRight(NodeImpl<E> parent, NodeImpl<E> child) {
        parent.setParent(child);
        if (child.getRightChild() != null) {
            child.getRightChild().setParent(parent);
        }
        parent.setLeftChild(child.getRightChild());
        child.setRightChild(parent);
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> pivot = validate(n);
        boolean isLeftChild = compare(n.getElement(), pivot.getParent().getElement()) < 0;
        relink(pivot.getParent(), pivot, isLeftChild);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo1811.okhanzhin.lesson7.Node)} to reduce the height of subtree rooted at <i>n1</i>
     *
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> parent = validate(parent(n));
        Node<E> subTreeRoot;

        if (parent.getLeftChild() != null) {
            boolean makeLeftChild = compare(parent.getElement(), parent.getParent().getElement()) < 0;
            relink(parent.getParent(), parent, makeLeftChild);
            subTreeRoot = parent;
        } else {
            rotate(n);
            boolean makeLeftChild = compare(parent.getParent().getElement(), validate(n).getParent().getElement()) < 0;
            relink(validate(n).getParent(), parent.getParent(), makeLeftChild);
            subTreeRoot = n;
        }

        return subTreeRoot;
    }
}
