package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.lang.String.CASE_INSENSITIVE_ORDER;

public class CityStorage {
    private NavigableSet<String> citySet = new TreeSet<>(CASE_INSENSITIVE_ORDER);

    public void add(String cityName) {
        citySet.add(cityName);
    }

    public String searchByPrefix(String prefix) {
        SortedSet<String> searchSet = citySet.subSet(prefix, true, prefix + (Character.MAX_VALUE), true);

        return searchSet.toString();
    }
}
