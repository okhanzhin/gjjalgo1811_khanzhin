package com.getjavajob.training.algo1811.okhanzhin.lesson7;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.binary.LinkedBinaryTree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("No Element!");
        }
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("No Element!");
        }
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return n.equals(root());
    }

    @Override
    public boolean isEmpty() {
        return root() == null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preOrder
     */
    public Collection<Node<E>> preOrder() {
        List<Node<E>> preOrderTraversalList = new ArrayList<>();
        if (root() == null) {
            return null;
        }
        LinkedBinaryTree.NodeImpl<E> cursor = (LinkedBinaryTree.NodeImpl<E>) root();
        populatePreOrder(cursor, preOrderTraversalList);

        return preOrderTraversalList;
    }

    private void populatePreOrder(LinkedBinaryTree.NodeImpl<E> cursor, List<Node<E>> list) {
        if (cursor != null) {
            list.add(cursor);
            populatePreOrder(cursor.getLeftChild(), list);
            populatePreOrder(cursor.getRightChild(), list);
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in postOrder
     */
    public Collection<Node<E>> postOrder() {
        List<Node<E>> postOrderTraversalList = new ArrayList<>();
        if (root() == null) {
            return null;
        }
        LinkedBinaryTree.NodeImpl<E> cursor = (LinkedBinaryTree.NodeImpl<E>) root();
        populatePostOrder(cursor, postOrderTraversalList);

        return postOrderTraversalList;
    }

    private void populatePostOrder(LinkedBinaryTree.NodeImpl<E> cursor, List<Node<E>> list) {
        if (cursor != null) {
            populatePostOrder(cursor.getLeftChild(), list);
            populatePostOrder(cursor.getRightChild(), list);
            list.add(cursor);
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        List<Node<E>> breathFirstTraversalList = new ArrayList<>();
        if (root() == null) {
            return null;
        } else {
            Queue<Node<E>> nodeQueue = new LinkedList<>();
            nodeQueue.add(root());
            while (nodeQueue.peek() != null) {
                LinkedBinaryTree.NodeImpl<E> cursor = (LinkedBinaryTree.NodeImpl<E>) nodeQueue.remove();
                breathFirstTraversalList.add(cursor);
                if (cursor.getLeftChild() != null) {
                    nodeQueue.add(cursor.getLeftChild());
                }
                if (cursor.getRightChild() != null) {
                    nodeQueue.add(cursor.getRightChild());
                }
            }
        }

        return breathFirstTraversalList;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private final Iterator<Node<E>> iterator = nodes().iterator();

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public E next() {
            return iterator.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
