package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import com.getjavajob.training.algo1811.okhanzhin.lesson5.CollectionUtils.UnmodifiableCollection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.algo1811.okhanzhin.lesson5.CollectionUtils.filter;
import static com.getjavajob.training.algo1811.okhanzhin.lesson5.CollectionUtils.transform;
import static com.getjavajob.training.algo1811.okhanzhin.lesson5.CollectionUtils.transformIntoNewCollection;
import static com.getjavajob.training.algo1811.okhanzhin.lesson5.CollectionUtils.forAllDo;
import static com.getjavajob.training.algo1811.okhanzhin.lesson5.CollectionUtils.unmodifiableCollection;
import static java.util.Arrays.asList;
import static util.Assert.assertBoolean;
import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class CollectionUtilsTest {
    public static void main(String[] args) {
        testFilter();
        testTransform();
        testTransformIntoNewCollection();
        testForAllDo();
        testUnmodifiableCollection();
    }

    public static List<Employee> initializeEmployeeList() {
        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee("Ivan", "Sokolov", 10_000));
        employees.add(new Employee("Sergey", "Ivanov", 15_000));
        employees.add(new Employee("Nikolay", "Dubcov", 100_000));
        employees.add(new Employee("Vasiliy", "Petrov", 30_000));

        return employees;
    }

    public static void testFilter() {
        List<Employee> employees = initializeEmployeeList();

        Filter<Employee> listFilter = new Filter() {
            @Override
            public boolean checkElement(Object element) {
                return ((Employee) element).getName().toLowerCase().contains("ivan") ||
                        ((Employee) element).getSurname().toLowerCase().contains("ivan");
            }
        };

        assertBoolean("CollectionUtilsTest.testFilter", true, filter(employees, listFilter));
    }

    public static void testTransform() {
        List<Employee> employees = initializeEmployeeList();

        Transformer<Employee, String> listTransformer = new Transformer() {
            @Override
            public Object changeType(Object element) {
                return ((Employee) element).getSurname();
            }
        };

        transform(employees, listTransformer);

        assertObject("CollectionUtilsTest.testTransform",
                        asList("Sokolov", "Ivanov", "Dubcov", "Petrov").toString(),
                        employees.toString());
    }

    public static void testTransformIntoNewCollection() {
        List<Employee> employees = initializeEmployeeList();

        Transformer<Employee, String> listTransformer = new Transformer() {
            @Override
            public Object changeType(Object element) {
                return ((Employee) element).getSurname();
            }
        };

        assertObject("CollectionUtilsTest.testTransformIntoNewCollection",
                        asList("Sokolov", "Ivanov", "Dubcov", "Petrov").toString(),
                        transformIntoNewCollection(employees, listTransformer).toString());
    }

    public static void testForAllDo() {
        List<Employee> employees = initializeEmployeeList();

        Closure<Employee> closure = new Closure() {
            @Override
            public void execute(Object element) {
                ((Employee) element).setSalary(((Employee) element).getSalary() * 2);
            }
        };

        List<String> resultList = new ArrayList<>(Arrays.asList("Employee{name='Ivan', surname='Sokolov', salary= 20000}",
                                                                "Employee{name='Sergey', surname='Ivanov', salary= 30000}",
                                                                "Employee{name='Nikolay', surname='Dubcov', salary= 200000}",
                                                                "Employee{name='Vasiliy', surname='Petrov', salary= 60000}"));

        assertObject("CollectionUtilsTest.testForAllDo", resultList.toString(), forAllDo(employees, closure).toString());
    }

    public static void testUnmodifiableCollection() {
        List<Employee> employees = initializeEmployeeList();

        UnmodifiableCollection<Employee> collection = (UnmodifiableCollection<Employee>) unmodifiableCollection(employees);

        assertEquals("CollectionUtilsTest.testUnmodifiableCollection", 4, collection.size());

        try {
            collection.add(new Employee("Vladimir", "Novikov", 100_000));
        } catch (Exception e) {
            assertObject("CollectionUtilsTest.testUnmodifiableCollectionThrowsException",
                          "UnsupportedOperationException", e.getClass().getSimpleName());
        }
    }

    public static class Employee{
        private String name;
        private String surname;
        private int salary;

        public Employee(String name, String surname, int yearsOfExperience) {
            this.name = name;
            this.surname = surname;
            this.salary = yearsOfExperience;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

        @Override
        public String toString() {
            return "Employee{" + "name='" + name + '\'' + ", surname='" + surname + '\'' + ", salary= " + salary + '}';
        }
    }
}
