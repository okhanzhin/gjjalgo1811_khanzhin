package com.getjavajob.training.algo1811.okhanzhin.lesson8;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.Node;
import com.getjavajob.training.algo1811.okhanzhin.lesson7.binary.LinkedBinaryTree;

import java.util.Comparator;

public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    protected Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (val1 instanceof Comparable) {
            return ((Comparable) val1).compareTo(val2);
        } else if (comparator != null) {
            return comparator.compare(val1, val2);
        } else {
            throw new IllegalArgumentException("Type must implement Comparable");
        }
    }

    @Override
    public Node<E> add(Node<E> n, E element) throws IllegalArgumentException {
        int resultCompare = compare(element, n.getElement());
        NodeImpl<E> currentNode = validate(n);
        if (resultCompare > 0) {
            if (currentNode.getRightChild() == null) {
                n = addRight(n, element);
            } else {
                n = add(currentNode.getRightChild(), element);
            }
        } else if (resultCompare < 0) {
            if (currentNode.getLeftChild() == null) {
                n = addLeft(n, element);
            } else {
                n = add(currentNode.getLeftChild(), element);
            }
        }

        return n;
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        NodeImpl<E> cursor = (NodeImpl<E>) n;

        while (cursor.getElement() != val) {
            if (compare(val, cursor.getElement()) < 0) {
                cursor = cursor.getLeftChild();
            }
            else {
                cursor = cursor.getRightChild();
            }
            if (cursor == null) {
                return null;
            }
        }

        return cursor;
    }

    protected void afterElementRemoved(Node<E> n) {
    }

    protected void afterElementAdded(Node<E> n) {
    }

    @Override
    public String toString() {
        NodeImpl<E> root = validate(root());
        return printToString(root);
    }

    private String printToString(NodeImpl<E> n) {
        StringBuilder sb = new StringBuilder();
        if (n != null) {
            sb.append("(").append(n.getElement()).append(printToString((n.getLeftChild()))).append(printToString(n.getRightChild())).append(")");
        }

        return sb.toString();
    }
}