package com.getjavajob.training.algo1811.okhanzhin.lesson5;

public interface Stack<V> {

    void push(V element);

    V pop();
}
