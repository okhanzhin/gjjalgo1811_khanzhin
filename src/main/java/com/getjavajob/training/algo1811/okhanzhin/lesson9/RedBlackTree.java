package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.Node;
import com.getjavajob.training.algo1811.okhanzhin.lesson8.balanced.BalanceableTree;

import java.util.Comparator;

public class RedBlackTree<E> extends BalanceableTree<E> {
    public RedBlackTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    private RedBlackNode<E> validateRedBlack(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        } else if (!(n instanceof RedBlackNode)) {
            throw new IllegalArgumentException("Node isn't instance of RedBlackNode and can't be cast.");
        }

        return (RedBlackNode<E>) n;
    }

    private boolean isBlack(Node<E> n) {
        return validateRedBlack(n).getBlack();
    }

    private boolean isRed(Node<E> n) {
        return !validateRedBlack(n).getBlack();
    }

    private void makeBlack(Node<E> n) {
        validateRedBlack(n).setBlack(true);
    }

    private void makeRed(Node<E> n) {
        validateRedBlack(n).setBlack(false);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        RedBlackNode<E> root = new RedBlackNode<>(e);
        makeBlack(root);
        setRoot(root);
        int size = size();
        setSize(++size);
        return root;
    }

    @Override
    public void addRoot(Node<E> n) {
        setRoot((NodeImpl<E>) n);
        makeBlack(n);
    }

    @Override
    public Node<E> add(Node<E> n, E element) throws IllegalArgumentException {
        n = super.add(n, element);
        afterElementAdded(n);
        return n;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parentNode = validate(n);
        RedBlackNode<E> newLeftNode = new RedBlackNode<>(e);
        makeRed(newLeftNode);
        parentNode.setLeftChild(newLeftNode);
        newLeftNode.setParent(parentNode);
        int size = size();
        setSize(++size);
        return newLeftNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parentNode = validate(n);
        RedBlackNode<E> newRightNode = new RedBlackNode<>(e);
        makeRed(newRightNode);
        parentNode.setRightChild(newRightNode);
        newRightNode.setParent(parentNode);
        int size = size();
        setSize(++size);
        return newRightNode;
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        NodeImpl<E> cursor = validate(n);
        RedBlackNode<E> cursorRedBlack = validateRedBlack(n);
        if (isRoot(n)) {
            makeBlack(n);
        } else {
            if (!validateRedBlack(cursor.getParent()).getBlack()) {
                RedBlackNode<E> uncle = getUncle(validate(n));
                if (uncle != null && !uncle.getBlack()) {
                    makeBlack(uncle);
                    makeBlack(cursor.getParent());
                    RedBlackNode<E> grandparent = getGrandparent(cursorRedBlack);
                    makeRed(grandparent);
                    afterElementAdded(grandparent);
                } else {
                    NodeImpl<E> grandparent = validate(getGrandparent(cursorRedBlack));
                    if (n.equals(cursor.getParent().getRightChild()) && cursor.getParent().equals(grandparent.getLeftChild())) {
                        rotate(n);
                        n = validateRedBlack(cursor.getLeftChild());
                    } else if (n.equals(cursor.getParent().getLeftChild()) && cursor.getParent().equals(grandparent.getRightChild())) {
                        rotate(n);
                        n = validateRedBlack(cursor.getRightChild());
                    }
                    NodeImpl<E> anotherGrandparent = getGrandparent(validateRedBlack(n));
                    makeBlack(validate(n).getParent());
                    makeRed(anotherGrandparent);
                    rotate(validate(n).getParent());
                }
            }
        }
    }

    private RedBlackNode<E> getUncle(NodeImpl<E> node) {
        RedBlackNode<E> grandparent = getGrandparent(node);
        if (grandparent == null) {
            return null;
        }
        if (node.getParent().equals(grandparent.getLeftChild())) {
            return grandparent.getRightChild();
        } else {
            return grandparent.getLeftChild();
        }
    }

    private RedBlackNode<E> getGrandparent(NodeImpl<E> node) {
        if ((node != null) && node.getParent() != null) {
            return validateRedBlack(node.getParent().getParent());
        } else {
            return null;
        }
    }

    private RedBlackNode<E> getBrother(NodeImpl<E> node) {
        NodeImpl<E> parent = node.getParent();
        if (parent == null) {
            return null;
        }
        if (node.equals(parent.getLeftChild())) {
            return validateRedBlack(parent.getLeftChild());
        } else {
            return validateRedBlack(parent.getRightChild());
        }
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int size = size();
        if (size == 0) {
            throw new IllegalArgumentException("Tree Is Empty!");
        }

        RedBlackNode<E> cursor = validateRedBlack(n).cloneNode(validateRedBlack(n));
        int childrenNumber = childrenNumber(cursor);
        boolean isRepairRequired = true;

        if (!cursor.getBlack() && childrenNumber == 2 && (cursor.getRightChild().getLeftChild() != null || cursor.getLeftChild().getRightChild() != null)) {
            isRepairRequired = false;
        }
        E removableElement = super.remove(n);

        if (isRepairRequired) {
            afterElementRemoved(cursor);
        }

        return removableElement;
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        RedBlackNode<E> cursor = validateRedBlack(n);

        if (!cursor.getBlack() && cursor.getRightChild() != null && cursor.getLeftChild() != null) {
            makeBlack(cursor.getLeftChild().getParent());
            makeRed(cursor.getRightChild());
        } else if (cursor.getBlack()) {
            if (childrenNumber(cursor) == 1) {
                if (cursor.getRightChild() != null) {
                    makeBlack(cursor.getRightChild());
                } else {
                    makeBlack(cursor.getLeftChild());
                }
            } else if (isExternal(cursor)) {
                NodeImpl<E> brother = getBrother(cursor);
                if (isRed(brother)) {
                    if (left(brother) != null) {
                        makeRed(left(brother));
                    }
                    rotate(brother);
                } else if (isExternal(brother)) {
                    makeRed(brother);
                    makeBlack(parent(cursor));
                } else if (left(brother) != null && right(brother) != null && isRed(left(brother)) && isBlack(right(brother))) {
                    rotate(left(brother));
                    makeBlack(brother);
                    makeRed(right(brother));
                } else if (left(brother) != null && right(brother) != null && isRed(right(brother))) {
                    makeBlack(right(brother));
                    rotate(brother);
                    makeBlack(cursor.getParent());
                }
            }
        }
    }

    @Override
    public String toString() {
        return "(" + printToString((RedBlackNode<E>) root()) + ")";
    }

    private String printToString(RedBlackNode<E> node) {
        StringBuilder result = new StringBuilder();
        String color;
        if (node == null) {
            return result.toString();
        }
        if (node != root() && node.getParent().getLeftChild() == node) {
            result.append("(");
        }
        color = node.getBlack() ? "B" : "R";
        result.append(node.getElement()).append(" - ").append(color);
        if (node.getLeftChild() != null || node.getRightChild() != null) {
            result.append(printToString(node.getLeftChild())).append(", ").append(printToString(node.getRightChild()));
            result.append(")");
        }
        return result.toString();
    }

    private static class RedBlackNode<E> extends NodeImpl<E> {

        private boolean isBlack;

        private RedBlackNode(E element) {
            super(element);
        }

        public boolean getBlack() {
            return isBlack;
        }

        public void setBlack(boolean isBlack) {
            this.isBlack = isBlack;
        }

        public RedBlackNode<E> getLeftChild() {
            return (RedBlackNode<E>) super.getLeftChild();
        }

        void setLeftChild(RedBlackNode<E> left) {
            super.setLeftChild(left);
        }

        public RedBlackNode<E> getRightChild() {
            return (RedBlackNode<E>) super.getRightChild();
        }

        void setRightChild(RedBlackNode<E> right) {
            super.setRightChild(right);
        }

        RedBlackNode<E> cloneNode(RedBlackNode<E> node) {
            RedBlackNode<E> clone = new RedBlackNode<>(node.getElement());
            clone.setBlack(node.getBlack());
            if (node.getLeftChild() != null) {
                clone.setLeftChild(node.cloneNode(node.getLeftChild()));
            }
            clone.setParent(node.getParent());
            if (node.getRightChild() != null) {
                clone.setRightChild(node.cloneNode(node.getRightChild()));
            }
            return clone;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (!(obj instanceof RedBlackNode)) return false;
            if (!super.equals(obj)) return false;
            RedBlackNode<?> rbNode = (RedBlackNode<?>) obj;
            return isBlack == rbNode.isBlack;
        }
    }
}
