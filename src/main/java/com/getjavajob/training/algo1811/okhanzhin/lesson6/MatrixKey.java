package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.Objects;

public class MatrixKey<V> {
    private int i;
    private int j;
    private int hash;

    public MatrixKey(int i, int j) {
        this.i = i;
        this.j = j;
        this.hash = hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hash(i, j);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != this.getClass()) {
            MatrixKey<?> matrix = (MatrixKey<?>) obj;
            if (matrix != null) {
                return i == matrix.i && j == matrix.j;
            }
        }

        return false;
    }
}
