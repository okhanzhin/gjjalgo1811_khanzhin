package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import static util.Assert.assertObject;

public class AssociativeArrayTest {
    public static void main(String[] args) {
        testPut();
        testPutReturnOldValue();
        testPutNullKey();
        testRemove();
        testRemoveNullKey();
        testGet();
        testGetNullKey();
        testSpecialKeys();
    }

    public static void testPut() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");
        testMap.put(4, "Four");
        testMap.put(5, "Five");
        testMap.put(33, "Blabla");

        assertObject("AssociativeArrayTest.testPut", "One", testMap.get(1));
    }

    public static void testPutReturnOldValue() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("AssociativeArrayTest.testPutReturnOldValue", "One", testMap.put(1, "Two"));
    }

    public static void testPutNullKey() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");
        testMap.put(null, "NULL");

        assertObject("AssociativeArrayTest.testPutNullKey", "NULL", testMap.get(null));
    }

    public static void testRemove() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("AssociativeArrayTest.testRemove", "Two", testMap.remove(2));
    }

    public static void testRemoveNullKey() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");
        testMap.put(null, "NULL");

        assertObject("AssociativeArrayTest.testRemoveNullKey", "NULL", testMap.remove(null));
    }

    public static void testGet() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("AssociativeArrayTest.testGet", "Three", testMap.get(3));
    }

    public static void testGetNullKey() {
        AssociativeArray<Integer, String> testMap = new AssociativeArray<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");
        testMap.put(null, "NULL");

        assertObject("AssociativeArrayTest.testGetNullKey", "NULL", testMap.get(null));
    }


    public static void testSpecialKeys() {
        AssociativeArray<String, String> testMap = new AssociativeArray<>();

        testMap.put("polygenelubricants", "One");
        testMap.put("random", "Two");

        assertObject("AssociativeArrayTest.testSpecialKeys", "One", testMap.get("polygenelubricants"));
        assertObject("AssociativeArrayTest.testSpecialKeys", "Two", testMap.get("random"));
    }
}
