package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.HashMap;

public class HashMapMatrix<V> implements Matrix<Integer> {
    private HashMap<MatrixKey<V>, Integer> matrixData;

    public HashMapMatrix(HashMap<MatrixKey<V>, Integer> matrixData) {
        this.matrixData = matrixData;
    }

    @Override
    public Integer get(int i, int j) {
        return matrixData.get(new MatrixKey<V>(i, j));
    }

    @Override
    public void set(int i, int j, Integer value) {
        matrixData.put(new MatrixKey<>(i, j), value);
    }
}
