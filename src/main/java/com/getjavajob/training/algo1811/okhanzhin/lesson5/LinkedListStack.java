package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import java.util.Arrays;

public class LinkedListStack<V> implements Stack<V> {

    private static final int DEFAULT_CAPACITY = 0;
    private static final int POP_INDEX = 0;
    private DoublyLinkedList<V> listData;
    private int stackSize;

    public LinkedListStack() {
        this.listData = new DoublyLinkedList<>();
        stackSize = DEFAULT_CAPACITY;
    }

    @Override
    public void push(V element) {
        listData.add(element);
        stackSize++;
    }

    @Override
    public V pop() {
        checkStackHasElement();
        stackSize--;

        return listData.remove(POP_INDEX);
    }

    public int size() {
        return stackSize;
    }

    public void printStack() {
        System.out.println(Arrays.toString(listData.toArray()));
    }

    public Object[] asArray() {
        return listData.toArray();
    }

    private void checkStackHasElement() {
        if (stackSize == 0) {
            throw new IndexOutOfBoundsException("Stack has not elements.");
        }
    }
}
