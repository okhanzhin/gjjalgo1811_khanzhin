package com.getjavajob.training.algo1811.okhanzhin.lesson10;

public class MergeSort {
    public static int[] sort(int[] array) {
        long size = array.length;
        if (size < 10) {
            insertionSort(array);
        } else {
            recMergeSort(array, 0, array.length - 1);
        }

        return array;
    }

    private static void recMergeSort(int[] array, int left, int right) {
        if (left < right) {
            int median = left + (right - left) / 2;
            recMergeSort(array, left, median);
            recMergeSort(array, median + 1, right);
            merge(array, left, median, right);
        }
    }

    private static void merge(int[] array, int start, int medium, int end) {
        int highPointer = medium + 1;

        while (start <= medium && highPointer <= end) {
            if (array[start] <= array[highPointer]) {
                start++;
            } else {
                int value = array[highPointer];
                int index = highPointer;

                while (index != start) {
                    array[index] = array[index - 1];
                    index--;
                }
                array[start] = value;
                start++;
                medium++;
                highPointer++;
            }
        }
    }

    private static void insertionSort(int[] array) {
        for (int outerCount = 1; outerCount < array.length; outerCount++) {
            int temp = array[outerCount];
            int innerCount = outerCount;
            while (innerCount > 0 && array[innerCount - 1] >= temp) {
                array[innerCount] = array[innerCount - 1];
                --innerCount;
            }
            array[innerCount] = temp;
        }
    }
}
