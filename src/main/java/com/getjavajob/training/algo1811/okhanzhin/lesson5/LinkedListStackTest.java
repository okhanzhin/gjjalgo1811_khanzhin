package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;
import static util.Assert.fail;

public class LinkedListStackTest {
    private static final String FAIL_DESCRIPTION = "Exception Test Failed";
    private static final String INDEX_OUT_DESCRIPTION = "Stack has not elements.";

    public static void main(String[] args) {
        testPush();
        testPop();
        testPopBelowLowerLimit();
    }

    public static void testPush() {
        LinkedListStack<Integer> testStack = new LinkedListStack<>();

        try {
            testStack.push(10);
            testStack.push(20);
            testStack.push(30);
            testStack.push(40);
            testStack.push(50);

            assertEquals("LinkedListStackTest.testPush", new int[]{50, 40, 30, 20, 10}, testStack.asArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testPop() {
        LinkedListStack<Integer> testStack = new LinkedListStack<>();

        testStack.push(10);
        testStack.push(20);
        testStack.push(30);
        testStack.push(40);
        testStack.push(50);

        try {
            assertEquals("LinkedListStackTest.testPop", 50, testStack.pop());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testPopBelowLowerLimit() {
        LinkedListStack<Integer> testStack = new LinkedListStack<>();

        try {
            testStack.push(10);
            testStack.pop();
            testStack.pop();

        } catch (IndexOutOfBoundsException e1) {
            assertObject("LinkedListStackTest.testPopBelowLowerLimit", INDEX_OUT_DESCRIPTION, e1.getMessage());
        }
    }
}
