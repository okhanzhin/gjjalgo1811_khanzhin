package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;
import static util.Assert.fail;

public class LinkedListQueueTest {
    private static final String FAIL_DESCRIPTION = "Exception Test Failed";
    private static final String INDEX_OUT_DESCRIPTION = "Queue has not elements.";

    public static void main(String[] args) {
        testAdd();
        testRemove();
        testRemoveBelowLowerLimit();
    }

    public static void testAdd() {
        LinkedListQueue<Integer> testQueue = new LinkedListQueue<>();

        try {
            testQueue.add(1);
            testQueue.add(2);
            testQueue.add(3);
            testQueue.add(4);

            assertEquals("LinkedListStackTest.testAdd", new int[]{1, 2, 3, 4}, testQueue.asArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testRemove() {
        LinkedListQueue<Integer> testQueue = new LinkedListQueue<>();

        try {
            testQueue.add(1);
            testQueue.add(2);
            testQueue.add(3);
            testQueue.add(4);

            testQueue.remove();
            testQueue.remove();

            assertEquals("LinkedListStackTest.testRemove", new int[]{3, 4}, testQueue.asArray());
            fail(FAIL_DESCRIPTION);
        } catch (AssertionError e) {
            assertEquals(FAIL_DESCRIPTION, e.getMessage());
        }
    }

    public static void testRemoveBelowLowerLimit() {
        LinkedListQueue<Integer> testQueue = new LinkedListQueue<>();

        try {
            testQueue.add(1);
            testQueue.add(2);
            testQueue.add(3);

            testQueue.remove();
            testQueue.remove();
            testQueue.remove();
            testQueue.remove();

        } catch (IndexOutOfBoundsException e1) {
            assertObject("LinkedListStackTest.testRemoveBelowLowerLimit", INDEX_OUT_DESCRIPTION, e1.getMessage());
        }
    }
}
