package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static util.Assert.assertBoolean;
import static util.Assert.assertObject;

public class LinkedHashSetTest {
    public static void main(String[] args) {
        testNoDuplicatesAdd();
        testOrderedAdd();
    }

    public static void testNoDuplicatesAdd() {
        Set<Integer> testSet = new LinkedHashSet();

        testSet.add(1);

        assertBoolean("LinkedHashSetTest.testNoDuplicatesAdd", false, testSet.add(1));
    }

    public static void testOrderedAdd() {
        Set<Integer> testSet = new LinkedHashSet();

        testSet.add(1);
        testSet.add(2);
        testSet.add(3);
        testSet.add(4);
        testSet.add(5);

        Iterator setIterator = testSet.iterator();

        setIterator.next();
        setIterator.next();
        setIterator.next();

        assertObject("LinkedHashSetTest.testOrderedAdd", 4, setIterator.next());
    }
}
