package com.getjavajob.training.algo1811.okhanzhin.lesson1;

import java.util.Arrays;

import static java.lang.Integer.parseInt;

public class Task6 {
    public static void main(String[] args) {
//        System.out.println(raiseToPower(4));
//        System.out.println(additionOfDegrees(1, 2));
//        System.out.println(resetLowerBits(0b01100001, 1));

        System.out.println(Arrays.toString(returnLowerBits(0b01100001, 6)));
    }

    /**
     * Calculate 2^n
     *
     * @param exponent - the degree which the number must be raised.
     * @return 2 in the degree of exponent.
     */
    public static int raiseToPower(int exponent) {
        if (exponent > 0 && exponent < 31) {
            return 1 << exponent;
        } else {
            System.out.println("Please enter correct exponent.");
            return -1;
        }
    }

    /**
     * Calculate 2^n + 2^m
     *
     * @param exponentN - first degree which the number must be raised.
     * @param exponentM - second degree which the number must be raised.
     * @return sum of degrees.
     */
    public static int additionOfDegrees(int exponentN, int exponentM) {
        if (exponentN > 0 && exponentN < 31 || exponentM > 0 && exponentM < 31) {
            return 1 << exponentN | 1 << exponentM;
        } else {
            System.out.println("Please enter correct exponent.");
            return -1;
        }
    }

    /**
     * Reset n lower bits
     *
     * @param inputNumber  - bit reset number.
     * @param numberOfBits - number of bits for reset.
     * @return number with a given number of bits reset.
     */
    public static int resetLowerBits(int inputNumber, int numberOfBits) {
        if (numberOfBits > 0) {
            int mask = -1 << numberOfBits;
            return inputNumber & mask;
        } else {
            System.out.println("Please enter correct number of bits.");
            return -1;
        }
    }

    /**
     * Set number's n-th bit with 1
     *
     * @param inputNumber - bit set number.
     * @param numberOfBit - bit number to set.
     * @return number with given number of bit set to 1.
     */
    public static int setBitWithOne(int inputNumber, int numberOfBit) {
        if (numberOfBit > 0) {
            int mask = 1 << numberOfBit--;
            return inputNumber | mask;
        } else {
            System.out.println("Please enter correct number of bits.");
            return -1;
        }
    }

    /**
     * Invert n-th bit (use 2 bit ops)
     *
     * @param inputNumber - bit invert number.
     * @param numberOfBit - bit number to invert.
     * @return number with given number of bit invert.
     */
    public static int invertBit(int inputNumber, int numberOfBit) {
        if (numberOfBit > 0) {
            int mask = 1 << numberOfBit;
            return inputNumber ^ mask;
        } else {
            System.out.println("Please enter correct number of bits.");
            return -1;
        }
    }

    /**
     * Set number's n-th bit with 0
     *
     * @param inputNumber - bit set number.
     * @param numberOfBit - bit number to set.
     * @return number with given number of bit set to 0.
     */
    public static int setBitWithZero(int inputNumber, int numberOfBit) {
        if (numberOfBit > 0) {
            int mask = 1 << numberOfBit--;
            return inputNumber & ~ mask;
        } else {
            System.out.println("Please enter correct number of bits.");
            return -1;
        }
    }

    /**
     * Return n lower bits
     *
     * @param inputNumber - bit return number.
     * @param numberOfBit - number of returning lower bits.
     * @return specified amount of lower bits.
     */
    public static int[] returnLowerBits(int inputNumber, int numberOfBit) {
        if (numberOfBit > 0) {
            int[] outputBits = new int[numberOfBit];
            for (int i = 0; i < numberOfBit; i++) {
                if ((inputNumber & (1 << i)) == 0){
                    outputBits[numberOfBit - i - 1] = 0;
                } else{
                    outputBits[numberOfBit - i - 1] = 1;
                }
            }
            return outputBits;
        } else {
            System.out.println("Please enter correct number of bits.");
            return null;
        }
    }

    /**
     * Return n-th bit
     *
     * @param inputNumber - bit return number.
     * @param numberOfBit - number of returning bit.
     * @return the specified bit of input number.
     */
    public static int returnBit(int inputNumber, int numberOfBit) {
        if (numberOfBit > 0) {
            int mask = inputNumber >> (numberOfBit - 1);
            return mask & 1;
        } else {
            System.out.println("Please enter correct number of bits.");
            return -1;
        }
    }

    /**
     * Given byte a. output bin representation using bit ops
     *
     * @param inputNumber - number for bin representation.
     * @return bin representation of input number.
     */
    public static StringBuilder convertToBin(byte inputNumber) {
        StringBuilder binBuffer = new StringBuilder();

        for (int i = 7; i >= 0; i--) {
            if (((1 << i) & inputNumber) != 0) {
                binBuffer.append('1');
            } else {
                binBuffer.append('0');
            }
        }

        return binBuffer;
    }
}
