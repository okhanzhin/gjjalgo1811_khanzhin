package com.getjavajob.training.algo1811.okhanzhin.lesson1;

import static com.getjavajob.training.algo1811.okhanzhin.lesson1.Task7.*;
import static util.Assert.assertEquals;

public class Task7Test {
    public static void main(String[] args) {
        testSwapVarsBitwiseCaseOne();
        testSwapVarsBitwiseCaseTwo();

        testSwapVarsArithmeticCaseOne();
        testSwapVarsArithmeticCaseTwo();
    }

    public static void testSwapVarsBitwiseCaseOne() {
        assertEquals("Task7Test.testSwapVarsBitwiseCaseOne", new int[]{25, 15}, swapVarsBitwiseCaseOne(15,25));
    }

    public static void testSwapVarsBitwiseCaseTwo() {
        assertEquals("Task7Test.testSwapVarsBitwiseCaseOne", new int[]{25, 15}, swapVarsBitwiseCaseOne(15,25));
    }

    public static void testSwapVarsArithmeticCaseOne() {
        assertEquals("Task7Test.testSwapVarsBitwiseCaseOne", new int[]{25, 15}, swapVarsBitwiseCaseOne(15,25));
    }

    public static void testSwapVarsArithmeticCaseTwo() {
        assertEquals("Task7Test.testSwapVarsBitwiseCaseOne", new int[]{25, 15}, swapVarsBitwiseCaseOne(15,25));
    }
}
