package com.getjavajob.training.algo1811.okhanzhin.lesson10;

import static com.getjavajob.training.algo1811.okhanzhin.lesson10.InsertionSort.sort;
import static util.Assert.assertEquals;

public class InsertionSortTest {
    public static void main(String[] args) {
        testSort();
    }

    public static void testSort() {
        int[] array = new int[]{1, 6, 2, 90, 7, 8, 0};
        int[] exceptedArray = new int[]{0, 1, 2, 6, 7, 8, 90};

        assertEquals("InsertionSortTest.testSort", exceptedArray, sort(array));
    }
}
