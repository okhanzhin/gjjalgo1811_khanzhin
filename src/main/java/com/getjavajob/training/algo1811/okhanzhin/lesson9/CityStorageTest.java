package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import static util.Assert.assertObject;

public class CityStorageTest {
    public static void main(String[] args) {
        testSearchByPrefix();
        testSearchByPrefixCaseInsensitive();
        testSearchByPrefixWithMaxCharacterValueCaseOne();
        testSearchByPrefixWithMaxCharacterValueCaseTwo();
    }

    public static void testSearchByPrefix() {
        CityStorage testStorage = new CityStorage();

        testStorage.add("Moscow");
        testStorage.add("Mogilev");
        testStorage.add("New York");
        testStorage.add("London");

        assertObject("CityStorageTest.testSearchByPrefix", "[Mogilev, Moscow]", testStorage.searchByPrefix("Mo"));
    }

    public static void testSearchByPrefixCaseInsensitive() {
        CityStorage testStorage = new CityStorage();

        testStorage.add("Moscow");
        testStorage.add("Mogilev");
        testStorage.add("New York");
        testStorage.add("London");

        assertObject("CityStorageTest.testSearchByPrefix", "[Mogilev, Moscow]", testStorage.searchByPrefix("mo"));
    }


    public static void testSearchByPrefixWithMaxCharacterValueCaseOne() {
        CityStorage testStorage = new CityStorage();

        testStorage.add("Moscow");
        testStorage.add("Mogilev");
        testStorage.add("Mog\\uFFFFilev");
        testStorage.add("New York");
        testStorage.add("London");

        assertObject("CityStorageTest.testSearchByPrefixWithMaxCharacterValueCaseOne", "[Mog\\uFFFFilev]", testStorage.searchByPrefix("mog\\uFFFF"));
    }

    public static void testSearchByPrefixWithMaxCharacterValueCaseTwo() {
        CityStorage testStorage = new CityStorage();

        testStorage.add("Moscow");
        testStorage.add("Mogilev");
        testStorage.add("Mogilev\\uFFFF\\uFFFF");
        testStorage.add("New York");
        testStorage.add("London");

        assertObject("CityStorageTest.testSearchByPrefixWithMaxCharacterValueCaseOne", "[Mogilev\\uFFFF\\uFFFF]", testStorage.searchByPrefix("mogilev\\uFFFF\\uFFFF"));
    }
}