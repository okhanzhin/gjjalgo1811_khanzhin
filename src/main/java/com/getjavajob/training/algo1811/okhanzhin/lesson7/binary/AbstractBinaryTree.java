package com.getjavajob.training.algo1811.okhanzhin.lesson7.binary;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.AbstractTree;
import com.getjavajob.training.algo1811.okhanzhin.lesson7.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parentNode = parent(n);
        if (childrenNumber(parentNode) == 1) {
            return null;
        }
        if (left(parentNode).equals(n)) {
            return right(parentNode);
        } else {
            return left(parentNode);
        }
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        List<Node<E>> childrenNodes = new ArrayList<>();
        childrenNodes.add(left(n));
        childrenNodes.add(right(n));

        return childrenNodes;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        int childrenNumber = 0;

        if (left(n) != null && right(n) != null) {
            childrenNumber = 2;
        } else if (left(n) == null ^ right(n) == null) {
            childrenNumber++;
        }

        return childrenNumber;
    }

    /**
     *
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        List<Node<E>> inOrderTraversalList = new ArrayList<>();
        if (root() == null) {
            return null;
        }
        LinkedBinaryTree.NodeImpl<E> cursor = (LinkedBinaryTree.NodeImpl<E>) root();
        populateInOrder(cursor, inOrderTraversalList);

        return inOrderTraversalList;
    }

    private void populateInOrder(LinkedBinaryTree.NodeImpl<E> cursor, List<Node<E>> list) {
        if (cursor != null) {
            populateInOrder(cursor.getLeftChild(), list);
            list.add(cursor);
            populateInOrder(cursor.getRightChild(), list);
        }
    }
}

