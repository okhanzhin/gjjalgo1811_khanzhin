package com.getjavajob.training.algo1811.okhanzhin.lesson5;

public class LinkedListQueue<V> extends AbstractQueue<V> {

    private static final int DEFAULT_QUEUES_SIZE = 0;
    private Node<V> front;
    private Node<V> rear;
    private int queueSize;

    public LinkedListQueue() {
        queueSize = DEFAULT_QUEUES_SIZE;
    }

    @Override
    public boolean add(V val) {
        Node<V> newElement = new Node<>(val);
        if (front == null) {
            front = newElement;
        } else {
            rear.next = newElement;
        }
        rear = newElement;
        queueSize++;
        return true;
    }

    @Override
    public V remove() {
        checkQueueHasElement();
        V removedElement = front.val;
        if (front.next == null) {
            rear = null;
        } else {
            front.next.prev = null;
        }
        front = front.next;
        queueSize--;

        return removedElement;
    }

    public Object[] asArray() {
        Object[] result = new Object[queueSize];
        int i = 0;
        for (Node<V> cursor = front; cursor != null; cursor = cursor.next) {
            result[i++] = cursor.val;
        }

        return result;
    }

    private void checkQueueHasElement() {
        if (queueSize == 0) {
            throw new IndexOutOfBoundsException("Queue has not elements.");
        }
    }

    private static class Node<V> {
        private Node<V> next;
        private Node<V> prev;
        private V val;

        public Node(V val) {
            this.val = val;
        }
    }
}
