package com.getjavajob.training.algo1811.okhanzhin.lesson3;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DynamicArray<E> extends AbstractList<E> {

    private static final int DEFAULT_CAPACITY = 10;
    private E[] elementData;
    private int size;
    private int modCount;

    public DynamicArray(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.elementData = (E[]) new Object[initialCapacity];
    }

    public DynamicArray() {
        this(DEFAULT_CAPACITY);
    }

    public boolean add(E e) {
        ensureCapacity();
        elementData[size++] = e;
        modCount++;
        return true;
    }

    public void add(int index, E e) {
        checkIndexCorrectness(index);
        ensureCapacity();
        if (index != size) {
            System.arraycopy(elementData, index, elementData, index + 1, size - index);
        }
        elementData[index] = e;
        modCount++;
        size++;
    }

    public E set(int index, E e) {
        checkIndexCorrectness(index);
        E inputElement = elementData[index];
        elementData[index] = e;
        return inputElement;
    }

    public E get(int index) {
        checkIndexCorrectness(index);
        return elementData[index];
    }

    public E remove(int index) {
        checkIndexCorrectness(index);
        modCount++;
        E obj = elementData[index];
        System.arraycopy(elementData, index + 1, elementData, index, size - index - 1);
        size--;
        return obj;
    }

    public boolean remove(Object e) {
        modCount++;
        int index = indexOf(e);
        if (index == -1) {
            return false;
        }
        return remove(index) != null;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) return i;
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(elementData[i])) return i;
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        System.arraycopy(elementData, 0, array, 0, size);
        return array;
    }

    private void ensureCapacity() {
        try {
            if (size == elementData.length) {
                modCount++;
                E[] array = (E[]) new Object[(elementData.length + (elementData.length >> 1))];
                System.arraycopy(elementData, 0, array, 0, elementData.length);
                elementData = array;
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    private void checkIndexCorrectness(int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("Incorrect Index.");
        }
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    public class ListIteratorImpl implements ListIterator<E> {
        int currentElement;
        int modCount;

        public ListIteratorImpl() {
            this.modCount = DynamicArray.this.modCount;
        }

        public boolean hasNext() {
            return currentElement < size;
        }

        public E next() {
            checkConcurrentModification();
            if (!hasNext()) {
                throw new NoSuchElementException("No Next Element!");
            }

            return DynamicArray.this.get(currentElement++);
        }

        public boolean hasPrevious() {
            return currentElement > 0;
        }

        public E previous() {
            checkConcurrentModification();
            if (!hasPrevious()) {
                throw new NoSuchElementException("No Previous Element!");
            }

            return DynamicArray.this.get(--currentElement);
        }

        public int nextIndex() {
            return currentElement;
        }

        public int previousIndex() {
            return currentElement - 1;
        }

        public void set(E e) {
            checkConcurrentModification();
            DynamicArray.this.set(currentElement, e);
        }

        public void add(E e) {
            checkConcurrentModification();
            DynamicArray.this.add(currentElement++, e);
            modCount = DynamicArray.this.modCount;
        }

        public void remove() {
            checkConcurrentModification();
            DynamicArray.this.remove(currentElement);
            modCount = DynamicArray.this.modCount;
        }

        private void checkConcurrentModification() {
            if (modCount != DynamicArray.this.modCount) {
                throw new ConcurrentModificationException("Concurrent Modification.");
            }
        }
    }
}
