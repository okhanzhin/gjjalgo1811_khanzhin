package com.getjavajob.training.algo1811.okhanzhin.lesson10;

public class QuickSort {
    public static int[] sort(int[] array) {
        long size = array.length;
        if (size < 10) {
            insertionSort(array);
        } else {
            quickSort(array, 0, array.length - 1);
        }

        return array;
    }

    private static void quickSort(int[] array, int left, int right) {
        int median = partitionIt(array, left, right);
        if (left < median - 1) {
            quickSort(array, left, median - 1);
        }
        if (right > median) {
            quickSort(array, median, right);
        }
    }

    private static int partitionIt(int[] array, int left, int right) {
        int pivot = array[left];

        while (left <= right) {
            while (array[left] < pivot) {
                left++;
            }
            while (array[right] > pivot) {
                right--;
            }

            if (left <= right) {
                int tmp = array[left];
                array[left] = array[right];
                array[right] = tmp;
                left++;
                right--;
            }
        }
        return left;
    }

    private static void insertionSort(int[] array) {
        for (int outerCount = 1; outerCount < array.length; outerCount++) {
            int temp = array[outerCount];
            int innerCount = outerCount;
            while (innerCount > 0 && array[innerCount - 1] >= temp) {
                array[innerCount] = array[innerCount - 1];
                --innerCount;
            }
            array[innerCount] = temp;
        }
    }
}
