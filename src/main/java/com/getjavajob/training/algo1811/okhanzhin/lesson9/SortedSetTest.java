package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import java.util.SortedSet;
import java.util.TreeSet;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class SortedSetTest {
    public static void main(String[] args) {
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    public static SortedSet<Integer> initializeSet() {
        SortedSet<Integer> testSet = new TreeSet<>();

        testSet.add(1);
        testSet.add(2);
        testSet.add(3);
        testSet.add(4);
        testSet.add(5);

        return testSet;
    }

    public static void testSubSet() {
        SortedSet<Integer> set = initializeSet();

        assertObject("SortedSetTest.testSubSet", "[3, 4]", set.subSet(3, 5).toString());
    }

    public static void testHeadSet() {
        SortedSet<Integer> set = initializeSet();

        assertObject("SortedSetTest.testHeadSet", "[1, 2]", set.headSet(3).toString());
    }

    public static void testTailSet() {
        SortedSet<Integer> set = initializeSet();

        assertObject("SortedSetTest.testTailSet", "[4, 5]", set.tailSet(4).toString());
    }

    public static void testFirst() {
        SortedSet<Integer> set = initializeSet();

        assertEquals("SortedSetTest.testFirst", 1, set.first());
    }

    public static void testLast() {
        SortedSet<Integer> set = initializeSet();

        assertEquals("SortedSetTest.testLast", 5, set.last());
    }
}
