package com.getjavajob.training.algo1811.okhanzhin.lesson1;

import static com.getjavajob.training.algo1811.okhanzhin.lesson1.Task6.*;
import static com.getjavajob.training.algo1811.okhanzhin.lesson1.Task6.returnLowerBits;
import static util.Assert.assertEquals;
import static util.Assert.assertStringBuilder;


public class Task6Test {
    public static void main(String[] args) {
        testRaiseToPower();
        testRaiseToPowerBelowLowerLimit();
        testRaiseToPowerAboveUpperLimit();

        testAdditionOfDegrees();
        testAdditionOfDegreesBelowLowerLimit();
        testAdditionOfDegreesAboveUpperLimit();

        testResettingLowerBits();
        testResettingLowerBitsBelowLowerLimit();

        testSetBitWithOne();
        testSetBitWithOneBelowLowerLimit();

        testInvertBit();
        testInvertBitBelowLowerLimit();

        testSettingBitWithZero();
        testSettingBitWithZeroBelowLowerLimit();

        testReturningLowerBits();
        testReturningLowerBitsBelowLowerLimit();

        testReturnBit();
        testReturnBitBelowLowerLimit();

        testConvertToBin();
    }

    public static void testRaiseToPower() {
        assertEquals("Task6Test.testRaiseToPower", 0b00010000,
                                raiseToPower(4));
    }

    public static void testRaiseToPowerBelowLowerLimit() {
        assertEquals("Task6Test.testRaiseToPower", (byte) 0b11111111, raiseToPower(-1));
    }

    public static void testRaiseToPowerAboveUpperLimit() {
        assertEquals("Task6Test.testRaiseToPower", (byte) 0b11111111, raiseToPower(32));
    }

    public static void testAdditionOfDegrees() {
        assertEquals("Task6Test.testAdditionOfDegrees", 0b00000110,
                                additionOfDegrees(2, 1));
    }

    public static void testAdditionOfDegreesBelowLowerLimit() {
        assertEquals("Task6Test.testAdditionOfDegreesBelowLowerLimit", (byte) 0b11111111,
                                additionOfDegrees(-1, 32));
    }

    public static void testAdditionOfDegreesAboveUpperLimit() {
        assertEquals("Task6Test.testAdditionOfDegreesAboveUpperLimit", (byte) 0b11111111,
                                additionOfDegrees(32, -1));
    }

    public static void testResettingLowerBits() {
        assertEquals("Task6Test.testResettingLowerBits", 0b01110000,
                                resetLowerBits(0b01110001, 2));
    }

    public static void testResettingLowerBitsBelowLowerLimit() {
        assertEquals("Task6Test.testResettingLowerBitsBelowLowerLimit", (byte) 0b11111111,
                                resetLowerBits(0b01110001, -1));
    }

    public static void testSetBitWithOne() {
        assertEquals("Task6Test.testSetBitWithOne", 0b01110001,
                                setBitWithOne(0b01100001, 4));
    }

    public static void testSetBitWithOneBelowLowerLimit() {
        assertEquals("Task6Test.testSetBitWithOneBelowLowerLimit", (byte) 0b11111111,
                                setBitWithOne(0b01100001, -1));
    }

    public static void testInvertBit() {
        assertEquals("Task6Test.testInvertBit", 0b01100101,
                                invertBit(0b01100001, 2));
    }

    public static void testInvertBitBelowLowerLimit() {
        assertEquals("Task6Test.testInvertBitBelowLowerLimit", (byte) 0b11111111,
                                invertBit(0b01100001, -1));
    }

    public static void testSettingBitWithZero() {
        assertEquals("Task6Test.testSettingBitWithZero", 0b01000001,
                                setBitWithZero(0b01100001, 5));
    }

    public static void testSettingBitWithZeroBelowLowerLimit() {
        assertEquals("Task6Test.testSettingBitWithZeroBelowLowerLimit", (byte) 0b11111111,
                                setBitWithZero(0b01100001, -1));
    }

    public static void testReturningLowerBits() {
        assertEquals("Task6Test.testReturningLowerBits", new int[]{1, 0, 0, 0, 0, 1},
                                returnLowerBits(0b01100001, 6));
    }

    public static void testReturningLowerBitsBelowLowerLimit() {
        assertEquals("Task6Test.testReturningLowerBits.testReturningLowerBitsBelowLowerLimit", null,
                                returnLowerBits(0b01100001, -1));
    }

    public static void testReturnBit() {
        assertEquals("Task6Test.testReturningBit", 0,
                                returnBit(0b01100001, 3));
    }

    public static void testReturnBitBelowLowerLimit() {
        assertEquals("Task6Test.testReturnBitBelowLowerLimit", (byte) 0b11111111,
                                returnBit(0b01100001, -1));
    }

    public static void testConvertToBin() {
        assertStringBuilder("Task6Test.testConvertToBin", new StringBuilder("01100001"),
                                       convertToBin((byte) 97));
    }
}
