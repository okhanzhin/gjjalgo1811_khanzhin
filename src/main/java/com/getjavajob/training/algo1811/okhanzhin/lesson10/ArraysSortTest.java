package com.getjavajob.training.algo1811.okhanzhin.lesson10;

import java.util.Arrays;

import static util.Assert.assertBoolean;
import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class ArraysSortTest {
    public static void main(String[] args) {
        testSort();
        testBinarySearch();
        testFill();
        testCopyOf();
        testCopyOfRange();
        testDeepToString();
        testDeepEquals();
    }

    public static void testSort() {
        int[] actualArray = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        Arrays.sort(actualArray);
        assertEquals("ArraysSortTest.testSort", new int[]{3, 5, 7, 13, 32, 44, 55, 66, 88}, actualArray);
    }

    private static void testBinarySearch() {
        int[] actualArray = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        assertEquals("ArraysSortTest.testBinarySearch", 3, Arrays.binarySearch(actualArray, 13));
    }

    private static void testFill() {
        int[] actualArray = new int[10];
        Arrays.fill(actualArray, 0);
        assertEquals("ArraysSortTest.testFill", new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, actualArray);
    }

    private static void testCopyOf() {
        int[] actualArray = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        assertEquals("ArraysSortTest.testCopyOf", new int[]{3, 5, 7, 13, 66, 55, 88, 44, 32, 0}, Arrays.copyOf(actualArray, 10));
    }

    private static void testCopyOfRange() {
        int[] actualArray = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        assertEquals("ArraysSortTest.testCopyOfRange", new int[]{13, 66, 55}, Arrays.copyOfRange(actualArray, 3, 6));
    }

    private static void testDeepToString() {
        Object[] actualArray = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        assertObject("ArraysSortTest.testDeepToString", "[3, 5, 7, 13, 66, 55, 88, 44, 32]", Arrays.deepToString(actualArray));
    }

    private static void testDeepEquals() {
        Object[] array1 = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        Object[] array2 = {3, 5, 7, 13, 66, 55, 88, 44, 32};
        assertBoolean("ArraysSortTest.testDeepEquals", true, Arrays.deepEquals(array1, array2));
    }
}
