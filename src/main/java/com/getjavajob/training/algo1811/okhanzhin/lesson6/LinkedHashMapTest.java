package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.LinkedHashMap;
import java.util.Map;

import static util.Assert.assertObject;

public class LinkedHashMapTest {
    public static void main(String[] args) {
        testNoDuplicateKeysAdd();
        testOrderedAdd();
    }

    public static void testNoDuplicateKeysAdd() {
        Map<Integer, String> testMap = new LinkedHashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("LinkedHashMapTest.testNoDuplicateKeysAdd", "One", testMap.put(1, "Duplicate"));
    }

    public static void testOrderedAdd() {
        Map<Integer, String> testMap = new LinkedHashMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");

        assertObject("LinkedHashMapTest.testOrderedAdd", "[One, Two, Three]", testMap.values().toString());
    }
}
