package com.getjavajob.training.algo1811.okhanzhin.lesson7.binary;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.Node;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    // nonpublic utility

    private Node<E> root;
    private int size;

    protected void setRoot(NodeImpl<E> root) {
        this.root = root;
    }

    protected void setSize(int size) {
        this.size = size;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException("Node isn't instance of NodeImpl and can't be cast.");
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        root = new NodeImpl<>(e);
        size++;
        return root;
    }

    public void addRoot(Node<E> n) {
        root = n;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> newNode = new NodeImpl<>(e);

        if (validate(n).leftChild == null) {
            addLeft(n, e);
        } else if (validate(n).rightChild == null) {
            addRight(n, e);
        } else {
            throw new IllegalArgumentException();
        }

        return newNode;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> newNode = validate(n);
        newNode.leftChild = new NodeImpl<>(e);
        newNode.leftChild.parent = newNode;
        size++;

        return newNode.leftChild;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> newNode = validate(n);
        newNode.rightChild = new NodeImpl<>(e);
        newNode.rightChild.parent = newNode;
        size++;

        return newNode.rightChild;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> currentNode = validate(n);
        if (currentNode == null) {
            throw new IllegalArgumentException();
        }
        E oldNode = currentNode.getElement();
        currentNode.element = e;
        return oldNode;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int size = size();

        if (size == 0) {
            throw new IllegalArgumentException("Tree is empty.");
        }

        int childrenNum = childrenNumber(n);
        E removedElement = n.getElement();

        if (childrenNum == 0) {
            removeLeafNode(n);
        } else if (childrenNumber(n) == 1) {
            removeOneChildNode(n);
        } else {
            NodeImpl<E> cursor = validate(n);
            NodeImpl<E> checkNode = cursor.getLeftChild();

            if (cursor.getRightChild().getLeftChild() != null) {
                checkNode = cursor.getRightChild().getLeftChild();
            } else if (cursor.getLeftChild().getRightChild() != null) {
                checkNode = cursor.getLeftChild().getRightChild();
            }

            if (checkNode.getRightChild() == null) {
                cursor.setElement(checkNode.getElement());
                removeLeafNode(checkNode);
            } else {
                cursor.setElement(checkNode.getElement());
                removeOneChildNode(checkNode);
            }
        }

        setSize(--size);
        return removedElement;
    }

    protected void removeLeafNode(Node<E> n) {
        if (!isRoot(n)) {
            NodeImpl<E> parent = validate(parent(n));
            if (parent.leftChild != null && parent.leftChild.equals(n)) {
                parent.leftChild = null;
            } else {
                parent.rightChild = null;
            }
        } else {
            root = null;
        }
    }

    protected void removeOneChildNode(Node<E> n) {
        NodeImpl<E> node = validate(n);
        NodeImpl<E> newRoot = node.leftChild != null ? node.leftChild : node.rightChild;
        if (!isRoot(n)) {
            NodeImpl<E> parent = validate(parent(n));
            if (parent.leftChild != null && parent.leftChild.equals(n)) {
                parent.leftChild = newRoot;
            } else {
                parent.rightChild = newRoot;
            }
        } else {
            root = newRoot;
        }
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        return validate(p).leftChild;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        return validate(p).rightChild;
    }

    @Override
    public Node<E> root() {
        return size == 0 ? null : root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return validate(n).parent;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        List<E> list = new LinkedList<>();
        for (Node<E> node : nodes()) {
            list.add(node.getElement());
        }

        return list.iterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return inOrder();
    }

    public static class NodeImpl<E> implements Node<E> {
        private E element;
        private NodeImpl<E> leftChild;
        private NodeImpl<E> rightChild;
        private NodeImpl<E> parent;

        public NodeImpl(E element) {
            this.element = element;
        }

        @Override
        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public NodeImpl<E> getParent() {
            return parent;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public NodeImpl<E> getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(NodeImpl<E> leftChild) {
            this.leftChild = leftChild;
        }

        public NodeImpl<E> getRightChild() {
            return rightChild;
        }

        public void setRightChild(NodeImpl<E> rightChild) {
            this.rightChild = rightChild;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NodeImpl)) return false;
            NodeImpl<?> node = (NodeImpl<?>) obj;
            return Objects.equals(element, node.element);
        }
    }
}