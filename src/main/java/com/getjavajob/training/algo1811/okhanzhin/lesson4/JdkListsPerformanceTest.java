package com.getjavajob.training.algo1811.okhanzhin.lesson4;

import util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JdkListsPerformanceTest {
    private static final int ONE_MILLION_ITERATION = 1_000_000;
    private static final int ONE_HUNDRED_THOUSANDS = 100_000;
    private static final int TWENTY_FIVE_THOUSAND = 25_000;

    public static void main(String[] args) {
        testFirstAddArrayList();
        testFirstAddLinkedList();
        testMiddleAddArrayList();
        testMiddleAddLinkedList();
        testLastAddArrayList();
        testLastAddLinkedList();

        testFirstRemoveArrayList();
        testFirstRemoveLinkedList();
        testMiddleRemoveArrayList();
        testMiddleRemoveLinkedList();
        testLastRemoveArrayList();
        testLastRemoveLinkedList();

    }

    public static void testFirstAddArrayList() {
        StopWatch sw = new StopWatch();

        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            arrayList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            arrayList.add(0, i);
        }

        System.out.println("TestAddFirstArrayList - add 25 000 element to the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testFirstAddLinkedList() {
        StopWatch sw = new StopWatch();

        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            linkedList.add(0, i);
        }

        System.out.println("TestAddFirstLinkedList - add 25 000 element to the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testMiddleAddArrayList() {
        StopWatch sw = new StopWatch();

        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            arrayList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            arrayList.add(ONE_MILLION_ITERATION / 2, i);
        }

        System.out.println("TestAddFirstArrayList - add 25 000 element to the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testMiddleAddLinkedList() {
        StopWatch sw = new StopWatch();

        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            linkedList.add(ONE_MILLION_ITERATION / 2, i);
        }

        System.out.println("TestAddMiddleLinkedList - add 25 000 element to the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testLastAddArrayList() {
        StopWatch sw = new StopWatch();

        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            arrayList.add(i);
        }

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10; i++) {
            arrayList.add(i);
        }

        System.out.println("TestAddLastArrayList - add 10 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testLastAddLinkedList() {
        StopWatch sw = new StopWatch();

        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10; i++) {
            linkedList.add(i);
        }

        System.out.println("TestAddLastLinkedList - add 10 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testFirstRemoveArrayList() {
        StopWatch sw = new StopWatch();

        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            arrayList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            arrayList.remove(0);
        }

        System.out.println("TestRemoveFirstArrayList - remove 25 000 element from the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testFirstRemoveLinkedList() {
        StopWatch sw = new StopWatch();

        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            linkedList.remove(0);
        }

        System.out.println("TestRemoveFirstLinkedList - remove 25 000 element from the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testMiddleRemoveArrayList() {
        StopWatch sw = new StopWatch();

        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < ONE_HUNDRED_THOUSANDS * 3; i++) {
            arrayList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            arrayList.remove(ONE_HUNDRED_THOUSANDS / 2);
        }

        System.out.println("TestRemoveFirstArrayList - remove 25 000 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testMiddleRemoveLinkedList() {
        StopWatch sw = new StopWatch();

        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_HUNDRED_THOUSANDS; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            linkedList.remove(ONE_HUNDRED_THOUSANDS / 2);
        }

        System.out.println("TestRemoveMiddleLinkedList - remove 25 000 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testLastRemoveArrayList() {
        StopWatch sw = new StopWatch();

        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10; i++) {
            arrayList.add(i);
        }

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10 / 2; i++) {
            arrayList.remove(arrayList.size() - 1);
        }

        System.out.println("TestRemoveLastArrayList - remove 5 000 000 element from the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testLastRemoveLinkedList() {
        StopWatch sw = new StopWatch();

        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10 / 2; i++) {
            linkedList.remove(linkedList.size() - 1);
        }

        System.out.println("TestRemoveLastLinkedList - remove 5 000 000 element from the end of the list: " + sw.getElapsedTime() + " ms.");
    }
}
