package com.getjavajob.training.algo1811.okhanzhin.lesson4;

import static util.Assert.assertBoolean;
import static util.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class CollectionTest {
    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testContains();
        testToArray();
        testAdd();
        testRemove();
        testAddAll();
        testRemoveAll();
        testRetainAll();
        testClear();
        testRemoveAll();
        testContainsAll();
        testEquals();
        testHashCode();
    }

    public static List<Integer> initArrayList(){
        List<Integer> arrayList = new ArrayList<>();

        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);

        return arrayList;
    }

    public static void testSize() {
        List<Integer> currentList = initArrayList();

        assertEquals("CollectionTest.testSize" , 5, currentList.size());
    }

    public static void testIsEmpty() {
        List<Integer> currentList = initArrayList();

        assertBoolean("CollectionTest.testIsEmpty" , false, currentList.isEmpty());
    }

    public static void testContains() {
        List<Integer> currentList = initArrayList();

        assertBoolean("CollectionTest.testContains", true, currentList.contains(4));
    }

    public static void testToArray() {
        List<Integer> currentList = initArrayList();

        assertEquals("CollectionTest.testToArray", new int[]{1, 2, 3, 4, 5}, currentList.toArray());
    }

    public static void testAdd() {
        List<Integer> currentList = initArrayList();

        currentList.add(6);

        assertEquals("CollectionTest.testAdd", new int[]{1, 2, 3, 4, 5, 6}, currentList.toArray());
    }

    public static void testRemove() {
        List<Integer> currentList = initArrayList();

        currentList.remove(3);

        assertEquals("CollectionTest.testRemove", new int[]{1, 2, 3, 5}, currentList.toArray());
    }

    public static void testAddAll() {
        List<Integer> firstList = initArrayList();
        List<Integer> secondList = new ArrayList<>();

        assertBoolean("CollectionTest.testAddAll", true, secondList.addAll(firstList));
    }

    public static void testRemoveAll() {
        List<Integer> firstList = initArrayList();
        List<Integer> secondList = new ArrayList<>();

        secondList.add(4);
        secondList.add(5);

        assertBoolean("CollectionTest.testRemoveAll", true, firstList.removeAll(secondList));
    }

    public static void testRetainAll() {
        List<Integer> firstList = initArrayList();
        List<Integer> secondList = new ArrayList<>();

        secondList.add(4);
        secondList.add(5);

        assertBoolean("CollectionTest.testRetainAll", true, firstList.retainAll(secondList));
    }

    public static void testClear() {
        List<Integer> currentList = initArrayList();

        currentList.clear();

        assertEquals("CollectionTest.testClear", 0, currentList.size());
    }

    public static void testContainsAll() {
        List<Integer> firstList = initArrayList();
        List<Integer> secondList = new ArrayList<>();
        secondList.add(1);
        secondList.add(2);
        secondList.add(3);

        assertBoolean("CollectionTest.testContainsAll", true, firstList.containsAll(secondList));
    }

    public static void testEquals() {
        List<Integer> firstList = initArrayList();
        List<Integer> secondList = initArrayList();

        assertBoolean("CollectionTest.testEquals", true, firstList.equals(secondList));
    }

    public static void testHashCode() {
        List<Integer> firstList = initArrayList();
        List<Integer> secondList = initArrayList();

        assertEquals("CollectionTest.testHashCode", secondList.hashCode(), firstList.hashCode());
    }
}
