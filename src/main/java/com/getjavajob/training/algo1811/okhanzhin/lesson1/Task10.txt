
What will be the output (int)(char)(byte)-1

Iteration 1:

    Integer (-1) cast to byte type. In binary we get one byte (8 bit) representation of (-1): 11111111.

Iteration 2:

    Byte number extend to char type. First, the byte is converted to an int (4 bytes) via widening primitive conversion,

    11111111 becomes 11111111_11111111_11111111_11111111,

    and then the resulting int is converted to a char (2 bytes) by narrowing primitive conversion

    11111111_11111111_11111111_11111111 becomes 11111111_11111111.

Iteration 3:

    Char to int conversion,

    11111111_11111111_11111111_11111111 becomes 00000000_00000000_11111111_11111111.

    In integer type we get value 65535 in dec.

    (int)(char)(byte) -1 = 65535.