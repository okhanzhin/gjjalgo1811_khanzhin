package com.getjavajob.training.algo1811.okhanzhin.lesson4;

import util.StopWatch;

import java.util.LinkedList;
import java.util.List;

public class DoublyLinkedListPerformanceTest {
    private static final int ONE_MILLION_ITERATION = 1_000_000;
    private static final int TEN_THOUSANDS = 10_000;
    private static final int FIFTY_THOUSANDS = 50_000;
    private static final int FIVE_HUNDRED_THOUSANDS = 500_000;
    private static final int TWENTY_FIVE_THOUSAND = 25_000;

    public static void main(String[] args) {
        testBooleanAddDoublyLinkedList();
        testBooleanAddLinkedList();
        System.out.println("\n\n");

        testAddFirstDoublyLinkedList();
        testAddFirstLinkedList();
        System.out.println("\n\n");

        testAddMiddleDoublyLinkedList();
        testAddMiddleLinkedList();
        System.out.println("\n\n");

        testAddLastDoublyLinkedList();
        testAddLastLinkedList();
        System.out.println("\n\n");

        testBooleanRemoveDoublyLinkedList();
        testBooleanRemoveLinkedList();
        System.out.println("\n\n");

        testRemoveFirstDoublyLinkedList();
        testRemoveFirstLinkedList();
        System.out.println("\n\n");

        testRemoveMiddleDoublyLinkedList();
        testRemoveMiddleLinkedList();
        System.out.println("\n\n");


        testRemoveEndDoublyLInkedList();
        testRemoveEndLInkedList();
        System.out.println("\n\n");

    }

    public static void initStartDoublyLinkedList(List<Integer> doublyLinkedList) {
        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            doublyLinkedList.add(i);
        }
    }

    public static void initStartLinkedList(List<Integer> linkedList) {
        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            linkedList.add(i);
        }
    }

    public static void testBooleanAddDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 3; i++) {
            doublyLinkedList.add(i);
        }

        System.out.println("TestBooleanAddDoublyLinkedList - add 3 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testBooleanAddLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        initStartLinkedList(linkedList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 3; i++) {
            linkedList.add(i);
        }

        System.out.println("TestBooleanAddLinkedList - add 3 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddFirstDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 3; i++) {
            doublyLinkedList.add(0, i);
        }

        System.out.println("TestAddFirstDoublyLinkedList - add 3 000 000 element to the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddFirstLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new LinkedList<>();

        initStartLinkedList(arrayList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 3; i++) {
            arrayList.add(0, i);
        }

        System.out.println("TestAddFirstLinkedList - add 3 000 000 element to the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddMiddleDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < FIFTY_THOUSANDS; i++) {
            doublyLinkedList.add(TWENTY_FIVE_THOUSAND, i);
        }

        System.out.println("TestAddMiddleDoublyLinkedList - add 25 000 element to the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddMiddleLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        initStartDoublyLinkedList(linkedList);

        sw.start();

        for (int i = 0; i < FIFTY_THOUSANDS; i++) {
            linkedList.add(TWENTY_FIVE_THOUSAND, i);
        }

        System.out.println("TestAddFirstLinkedList - add 25 000 element to the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddLastDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 3; i++) {
            doublyLinkedList.add(doublyLinkedList.size(), i);
        }

        System.out.println("TestAddLastDoublyLinkedList - add 3 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddLastLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        initStartLinkedList(linkedList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 3; i++) {
            linkedList.add(linkedList.size(), i);
        }

        System.out.println("TestAddLastLinkedList - add 3 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testBooleanRemoveDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            doublyLinkedList.remove(doublyLinkedList.get(i));
        }

        System.out.println("TestBooleanAddDoublyLinkedList - add 25 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testBooleanRemoveLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        initStartLinkedList(linkedList);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            linkedList.remove(linkedList.get(i));
        }

        System.out.println("TestBooleanAddLinkedList - add 25 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveFirstDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS * 4; i++) {
            doublyLinkedList.remove(i);
        }

        System.out.println("TestRemoveFirstDoublyLinkedList - remove 40 000 element from the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveFirstLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        initStartLinkedList(linkedList);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS * 4; i++) {
            linkedList.remove(i);
        }

        System.out.println("TestRemoveFirstLinkedList - remove 40 000 element from the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveMiddleDoublyLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        initStartDoublyLinkedList(doublyLinkedList);

        sw.start();

        for (int i = 0; i < 1200; i++) {
            doublyLinkedList.remove(FIVE_HUNDRED_THOUSANDS);
        }

        System.out.println("TestRemoveMiddleDoublyLinkedList - remove 1200 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveMiddleLinkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        initStartLinkedList(linkedList);

        sw.start();

        for (int i = 0; i < 1200; i++) {
            linkedList.remove(FIVE_HUNDRED_THOUSANDS);
        }

        System.out.println("TestRemoveMiddleLinkedList - remove 1200 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveEndDoublyLInkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> doublyLinkedList = new DoublyLinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10; i++) {
            doublyLinkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 9; i++) {
            doublyLinkedList.remove(doublyLinkedList.size() - 1);
        }

        System.out.println("TestRemoveEndLinkedList - remove 9_000_000 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveEndLInkedList() {
        StopWatch sw = new StopWatch();
        List<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < ONE_MILLION_ITERATION * 10; i++) {
            linkedList.add(i);
        }

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 9; i++) {
            linkedList.remove(linkedList.size() - 1);
        }

        System.out.println("TestRemoveEndLinkedList - remove 9_000_000 element from the end of the list: " + sw.getElapsedTime() + " ms.");
    }
}
