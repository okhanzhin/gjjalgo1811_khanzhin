package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import com.getjavajob.training.algo1811.okhanzhin.lesson4.AbstractList;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<V> extends AbstractList<V> {

    private static final int DEFAULT_CAPACITY = 0;
    private int size;
    private Element<V> first;
    private Element<V> last;

    public DoublyLinkedList() {
        this.size = DEFAULT_CAPACITY;
    }

    public DoublyLinkedList(int size) {
        this.size = size;
    }

    @Override
    public boolean add(V val) {
        Element<V> newElement = new Element<>(null, null, val);

        if (first == null) {
            first = newElement;
            last = newElement;
        } else {
            newElement.next = first;
            first = newElement;
        }

        size++;
        return true;
    }

    @Override
    public void add(int index, V val) {
        checkIndexCorrectness(index);
        Element<V> newElement = new Element<>(null, null, val);

        if (first == null) {
            first = newElement;
            last = newElement;
        } else if (index == 0) {
            newElement.next = first;
            first = newElement;
        } else if (index == size) {
            newElement.prev = last;
            last.next = newElement;
            last = newElement;
        } else {
            Element<V> cursor = first;
            while (--index != 0) {
                cursor = cursor.next;
            }
            newElement.next = cursor.next;
            cursor.next = newElement;
        }

        size++;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (Element<V> cursor = first; cursor != null; cursor = cursor.next) {
                if (cursor.val == null) {
                    if (cursor == first) {
                        removeFirst();
                    } else if (cursor.next == null) {
                        removeLast();
                    } else {
                        cursor.prev.next = cursor.next;
                        cursor.next.prev = cursor.prev;
                    }
                    size--;
                    return true;
                }
            }
        } else {
            for (Element<V> cursor = first; cursor != null; cursor = cursor.next) {
                if (o.equals(cursor.val)) {
                    if (cursor == first) {
                        removeFirst();
                    } else if (cursor.next == null) {
                        removeLast();
                    } else {
                        cursor.prev.next = cursor.next;
                        cursor.next.prev = cursor.prev;
                    }
                    size--;
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public V remove(int index) {
        checkIndexCorrectness(index);

        Element<V> removedElement = element(index);
        final V elementValue = removedElement.val;
        final Element<V> next = removedElement.next;
        final Element<V> prev = removedElement.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            removedElement.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            removedElement.next = null;
        }

        removedElement.val = null;
        size--;
        return elementValue;
    }

    private Element<V> removeFirst() {
        Element<V> temp = first;

        if (first.next == null) {
            last = null;
        } else {
            first.next.prev = null;
        }
        first = first.next;

        return temp;
    }

    private Element<V> removeLast() {
        Element<V> temp = last;

        if (first.next == null) {
            last = null;
        } else {
            last.prev.next = null;
            last = last.prev;
        }

        return temp;
    }

    @Override
    public V get(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException("Incorrect Index.");
        }
        Element<V> outputElement = first;

        if (index <= size >> 1) {
            Element<V> cursor = first;
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    outputElement = cursor;
                    break;
                }
                cursor = cursor.next;
            }
        } else if (index > size >> 1) {
            Element<V> cursor = last;
            for (int i = size - 1; i > 0; i--) {
                if (i == index) {
                    outputElement = cursor;
                    break;
                }
                cursor = cursor.prev;
            }
        }

        return outputElement.val;
    }

    public int size() {
        return size;
    }

    Element<V> element(int index) {
        if (index < size >> 1) {
            Element<V> x = first;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
            return x;
        } else {
            Element<V> x = last;
            for (int i = size - 1; i > index; i--) {
                x = x.prev;
            }
            return x;
        }
    }

    private void checkIndexCorrectness(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Incorrect Index.");
        }
    }

    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;

        for (Element<V> cursor = first; cursor != null; cursor = cursor.next) {
            result[i++] = cursor.val;
        }

        return result;
    }

    @Override
    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    @Override
    public ListIteratorImpl listIterator(int index) {
        return new ListIteratorImpl(index);
    }

    private class Element<V> {
        private Element<V> prev;
        private Element<V> next;
        private V val;

        public Element(V val) {
            this.val = val;
        }

        public Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }

        public void print() {
            StringBuilder builder = new StringBuilder();
            System.out.print(builder.append(val).append(' '));
        }
    }

    public class ListIteratorImpl implements ListIterator<V> {
        private Element<V> lastReturnedElement;
        private Element<V> cursor = first;
        private int index;

        ListIteratorImpl() {
        }

        ListIteratorImpl(int index) {
            Element<V> initElement;

            if (index < size >> 1) {
                Element<V> x = first;
                for (int i = 0; i < index; i++)
                    x = x.next;
                initElement = x;
            } else {
                Element<V> x = last;
                for (int i = size - 1; i > 0; i--)
                    x = x.prev;
                initElement = x;
            }

            cursor = (index == size) ? null : initElement;
            this.index = index;
        }

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public V next() {
            if (!hasNext()) {
                throw new NoSuchElementException("No Next Element!");
            }

            lastReturnedElement = cursor;
            cursor = cursor.next;
            index++;

            return lastReturnedElement.val;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public V previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException("No Previous Element!");
            }

            lastReturnedElement = cursor = (cursor == null) ? last : cursor.prev;
            index--;

            return lastReturnedElement.val;
        }

        @Override
        public int nextIndex() {
            return index;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            if (lastReturnedElement == null) {
                throw new IllegalStateException("No Element To Remove.");
            }

            Element<V> lastNext = lastReturnedElement.next;
            Element<V> prev = lastReturnedElement.prev;
            Element<V> next = lastReturnedElement.next;

            if (prev == null) {
                first = next;
            } else {
                prev.next = next;
                lastReturnedElement.prev = null;
            }

            if (next == null) {
                last = prev;
            } else {
                next.prev = prev;
                lastReturnedElement.next = null;
            }

            lastReturnedElement.val = null;
            size--;

            if (cursor == lastReturnedElement) {
                cursor = lastNext;
            } else {
                index--;
            }

            lastReturnedElement = null;
        }

        @Override
        public void set(V val) {
            if (lastReturnedElement == null) {
                throw new IllegalStateException("No Element To Set.");
            }

            lastReturnedElement.val = val;
        }

        @Override
        public void add(V val) {
            lastReturnedElement = null;
            Element<V> newElement = new Element<>(val);
            Element<V> lastElement = last;

            if (cursor == null) {
                last = newElement;
                if (lastElement == null) {
                    first = newElement;
                } else {
                    lastElement.next = newElement;
                }
            } else {
                Element<V> previous = cursor.prev;
                newElement.prev = previous;
                newElement.next = cursor;
                cursor.prev = newElement;
                if (previous == null) {
                    first = newElement;
                } else {
                    previous.next = newElement;
                }
            }
            size++;
            index++;
        }
    }
}
