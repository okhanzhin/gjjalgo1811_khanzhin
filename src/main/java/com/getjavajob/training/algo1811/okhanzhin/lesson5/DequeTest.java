package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import static util.Assert.assertEquals;
import static util.Assert.assertNull;

public class DequeTest {
    private static final String NULL_POINTER_EXCEPTION = "NullPointerException";
    private static final String NO_SUCH_ELEMENT_EXCEPTION = "NoSuchElementException";

    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
    }

    public static Deque<Integer> initializeArrayDeque() {
        Deque<Integer> arrayDeque = new ArrayDeque<>();

        arrayDeque.add(0);
        arrayDeque.add(1);
        arrayDeque.add(2);
        arrayDeque.add(3);
        arrayDeque.add(4);

        return arrayDeque;
    }

    public static void testAddFirst() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        currentDeque.addFirst(100);

        assertEquals("DequeTest.testAddFirst", new int[]{100, 0, 1, 2, 3, 4}, currentDeque.toArray());

        try {
            currentDeque.addFirst(null);
        } catch (NullPointerException e) {
            assertEquals(NULL_POINTER_EXCEPTION, e.getClass().getSimpleName());
        }
    }

    public static void testAddLast() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        currentDeque.addLast(100);

        assertEquals("DequeTest.testAddLast", new int[]{0, 1, 2, 3, 4, 100}, currentDeque.toArray());

        try {
            currentDeque.addLast(null);
        } catch (NullPointerException e) {
            assertEquals(NULL_POINTER_EXCEPTION, e.getClass().getSimpleName());
        }
    }

    public static void testOfferFirst() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        currentDeque.offerFirst(100);

        assertEquals("DequeTest.testOfferFirst", new int[]{100, 0, 1, 2, 3, 4}, currentDeque.toArray());
    }

    public static void testOfferLast() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        currentDeque.offerLast(100);

        assertEquals("DequeTest.testOfferLast", new int[]{0, 1, 2, 3, 4, 100}, currentDeque.toArray());
    }

    public static void testRemoveFirst() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testRemoveFirst", 0, currentDeque.removeFirst());

        int startSize = currentDeque.size();

        for (int i = 0; i < startSize; i++) {
            currentDeque.removeFirst();
        }

        try {
            currentDeque.removeFirst();
        } catch (NoSuchElementException e) {
            assertEquals(NO_SUCH_ELEMENT_EXCEPTION, e.getClass().getSimpleName());
        }
    }

    public static void testRemoveLast() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testRemoveLast", 4, currentDeque.removeLast());

        int startSize = currentDeque.size();

        for (int i = 0; i < startSize; i++) {
            currentDeque.removeFirst();
        }

        try {
            currentDeque.removeFirst();
        } catch (NoSuchElementException e) {
            assertEquals(NO_SUCH_ELEMENT_EXCEPTION, e.getClass().getSimpleName());
        }
    }

    public static void testPollFirst() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testPollFirst", 0, currentDeque.pollFirst());

        int startSize = currentDeque.size();

        for (int i = 0; i < startSize; i++) {
            currentDeque.removeFirst();
        }

        assertNull("DequeTest.testPollFirstEmptyDeque", null, currentDeque.pollFirst());
    }

    public static void testPollLast() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testPollLast", 4, currentDeque.pollLast());

        int startSize = currentDeque.size();

        for (int i = 0; i < startSize; i++) {
            currentDeque.removeFirst();
        }

        assertNull("DequeTest.testPollLastEmptyDeque", null, currentDeque.pollLast());
    }

    public static void testGetFirst() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testGetFirst", 0, currentDeque.getFirst());

        currentDeque.clear();

        try {
            currentDeque.getFirst();
        } catch (NoSuchElementException e) {
            assertEquals(NO_SUCH_ELEMENT_EXCEPTION, e.getClass().getSimpleName());
        }
    }

    public static void testGetLast() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testGetLast", 4, currentDeque.getLast());

        currentDeque.clear();

        try {
            currentDeque.getFirst();
        } catch (NoSuchElementException e) {
            assertEquals(NO_SUCH_ELEMENT_EXCEPTION, e.getClass().getSimpleName());
        }
    }

    public static void testPeekFirst() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testPeekFirst", 0, currentDeque.peekFirst());

        currentDeque.clear();

        assertNull("DequeTest.testPeekFirstEmptyDeque", null, currentDeque.peekFirst());
    }

    public static void testPeekLast() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        assertEquals("DequeTest.testPeekLast", 4, currentDeque.peekLast());

        currentDeque.clear();

        assertNull("DequeTest.testPeekLastEmptyDeque", null, currentDeque.peekLast());
    }

    public static void testRemoveFirstOccurrence() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        currentDeque.add(2);
        currentDeque.removeFirstOccurrence(2);

        assertEquals("DequeTest.testRemoveFirstOccurrence", new int[]{0, 1, 3, 4, 2}, currentDeque.toArray());
    }

    public static void testRemoveLastOccurrence() {
        Deque<Integer> currentDeque = initializeArrayDeque();

        currentDeque.add(2);
        currentDeque.removeLastOccurrence(2);

        assertEquals("DequeTest.testRemoveLastOccurrence", new int[]{0, 1, 2, 3, 4}, currentDeque.toArray());
    }
}
