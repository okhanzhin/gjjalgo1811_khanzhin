package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.NoSuchElementException;

import static util.Assert.assertBoolean;
import static util.Assert.assertNull;
import static util.Assert.assertEquals;

public class QueueTest {
    private static final String NO_SUCH_ELEMENT = "NoSuchElementException";

    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
    }

    public static Queue<Integer> initializeArrayQueue() {
        Queue<Integer> arrayDequeQueue = new ArrayDeque<>();

        arrayDequeQueue.add(0);
        arrayDequeQueue.add(1);
        arrayDequeQueue.add(2);
        arrayDequeQueue.add(3);
        arrayDequeQueue.add(4);

        return arrayDequeQueue;
    }

    public static void testAdd() {
        Queue<Integer> currentQueue = initializeArrayQueue();

        assertBoolean("QueueTest.testAdd", true, currentQueue.add(5));

    }

    public static void testOffer() {
        Queue<Integer> currentQueue = initializeArrayQueue();

        assertBoolean("QueueTest.testOffer", true, currentQueue.add(5));
    }

    public static void testRemove() {
        Queue<Integer> currentQueue = initializeArrayQueue();

        assertEquals("QueueTest.testRemove", 0, currentQueue.remove());

        int startSize = currentQueue.size();

        for (int i = 0; i < startSize; i++) {
            currentQueue.remove();
        }

        try {
            currentQueue.remove();

        } catch (NoSuchElementException e) {
            assertEquals(NO_SUCH_ELEMENT, e.getClass().getSimpleName());
        }
    }

    public static void testPoll() {
        Queue<Integer> currentQueue = initializeArrayQueue();

        assertEquals("QueueTest.testPoll", 0, currentQueue.poll());

        int startSize = currentQueue.size();

        for (int i = 0; i < startSize; i++) {
            currentQueue.remove();
        }

        assertNull("QueueTest.testPollEmptyQueue", null, currentQueue.poll());
    }

    public static void testElement() {
        Queue<Integer> currentQueue = initializeArrayQueue();

        int startSize = currentQueue.size();

        assertEquals("QueueTest.testElement", 0, currentQueue.element());

        for (int i = 0; i < startSize; i++) {
            currentQueue.remove();
        }

        try {
            currentQueue.element();
        } catch (NoSuchElementException e) {
            assertEquals(NO_SUCH_ELEMENT, e.getClass().getSimpleName());
        }
    }

    public static void testPeek() {
        Queue<Integer> currentQueue = initializeArrayQueue();

        int startSize = currentQueue.size();

        assertEquals("QueueTest.testPeek", 0, currentQueue.peek());

        for (int i = 0; i < startSize; i++) {
            currentQueue.remove();
        }

        assertNull("QueueTest.testPeekEmptyQueue", null, currentQueue.peek());
    }
}
