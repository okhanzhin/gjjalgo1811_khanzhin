package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import java.util.TreeMap;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class NavigableMapTest {
    public static void main(String[] args) {
        testLowerEntry();
        testLowerKey();
        testFloorEntry();
        testFloorKey();
        testCeilingEntry();
        testCeilingKey();
        testHigherEntry();
        testHigherKey();
        testFirstEntry();
        testLastEntry();
        testPollFirstEntry();
        testPollLastEntry();
        testDescendingMap();
        testDescendingKeySet();
        testNavigableKeySet();
        testSubMap();
        testHeadMap();
        testTailMap();
    }

    public static TreeMap<Integer, String> initializeMap() {
        TreeMap<Integer, String> testMap = new TreeMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");
        testMap.put(4, "Four");
        testMap.put(5, "Five");
        testMap.put(6, "Six");
        testMap.put(7, "Seven");
        testMap.put(8, "Eight");
        testMap.put(9, "Nine");
        testMap.put(10, "Ten");
        testMap.put(11, "Eleven");
        testMap.put(12, "Twelve");

        return testMap;
    }

    public static void testLowerEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testLowerEntry","5=Five", map.lowerEntry(6).toString());
    }

    public static void testLowerKey() {
        TreeMap<Integer, String> map = initializeMap();

        assertEquals("NavigableMapTest.testLowerKey",5, map.lowerKey(6));
    }

    public static void testFloorEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testFloorEntry","6=Six", map.floorEntry(6).toString());
    }

    public static void testFloorKey() {
        TreeMap<Integer, String> map = initializeMap();

        assertEquals("NavigableMapTest.testFloorKey",6, map.floorKey(6));
    }

    public static void testCeilingEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testCeilingEntry","6=Six", map.ceilingEntry(6).toString());
    }

    public static void testCeilingKey() {
        TreeMap<Integer, String> map = initializeMap();

        assertEquals("NavigableMapTest.testCeilingKey",6, map.ceilingKey(6));
    }

    public static void testHigherEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testHigherEntry","7=Seven", map.higherEntry(6).toString());
    }

    public static void testHigherKey() {
        TreeMap<Integer, String> map = initializeMap();

        assertEquals("NavigableMapTest.testHigherKey",7, map.higherKey(6));
    }

    public static void testFirstEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testFirstEntry", "1=One", map.firstEntry().toString());
    }

    public static void testLastEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testLastEntry", "12=Twelve", map.lastEntry().toString());
    }

    public static void testPollFirstEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testPollFirstEntry", "1=One", map.pollFirstEntry().toString());
    }

    public static void testPollLastEntry() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testPollFirstEntry", "12=Twelve", map.pollLastEntry().toString());
    }

    public static void testDescendingMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testDescendingMap", "{12=Twelve, 11=Eleven, 10=Ten, 9=Nine, 8=Eight, 7=Seven, 6=Six, 5=Five, 4=Four, 3=Three, 2=Two, 1=One}", map.descendingMap().toString());
    }

    public static void testDescendingKeySet() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testDescendingKeySet", "[12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]", map.descendingKeySet().toString());
    }

        public static void testNavigableKeySet() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testNavigableKeySet", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]", map.navigableKeySet().toString());
    }

    public static void testSubMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testSubMap", "{5=Five, 6=Six, 7=Seven, 8=Eight, 9=Nine}", map.subMap(5, true, 9, true).toString());
    }

    public static void testHeadMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testHeadMap", "{1=One, 2=Two, 3=Three}", map.headMap(3, true).toString());
    }

    public static void testTailMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("NavigableMapTest.testTailMap", "{11=Eleven, 12=Twelve}", map.tailMap(10, false).toString());
    }
}
