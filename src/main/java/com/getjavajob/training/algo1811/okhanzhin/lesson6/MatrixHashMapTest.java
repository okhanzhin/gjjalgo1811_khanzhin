package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static util.Assert.assertEquals;

public class MatrixHashMapTest {
    private static final int ONE_MILLION = 1_000_000;

    public static void main(String[] args) {
        testSetAndGet();
    }

    public static void testSetAndGet() {
        HashMapMatrix<Integer> testMatrix = new HashMapMatrix<>(new HashMap<>());

        List<Integer> expected = new ArrayList<>();
        List<Integer> actual = new ArrayList<>();

        for (int i = 0; i < ONE_MILLION; i++) {
            testMatrix.set(i, ONE_MILLION, i);
            expected.add(i);
        }

        for (int i = 0; i < ONE_MILLION; i++) {
            actual.add(testMatrix.get(i, ONE_MILLION));
        }

        assertEquals("MatrixHashMapTest.testSetAndGet", expected.size(), actual.size());
    }
}
