package com.getjavajob.training.algo1811.okhanzhin.lesson8;

import java.util.Comparator;
import com.getjavajob.training.algo1811.okhanzhin.lesson7.binary.LinkedBinaryTree.NodeImpl;

import static util.Assert.assertBoolean;
import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class BinarySearchTreeTest {
    public static void main(String[] args) {
        testCompare();
        testTreeSearch();
        testAdd();
        testRemoveLeaf();
        testRemoveOneChildNode();
        testRemoveTwoChildrenNode();
        testRemoveRoot();
        testToString();
    }

    public static void testCompare() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        assertBoolean("BinarySearchTreeTest.testCompare", true, bstTest.compare(4, 8) < 0);
    }

    public static void testTreeSearch() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(10);

        bstTest.add(root, 1);
        bstTest.add(root, 100);
        bstTest.add(root, 0);

        assertEquals("BinarySearchTreeTest.testTreeSearch", 0, bstTest.treeSearch(bstTest.root(), 0).getElement());
    }

    public static void testAdd() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(10);

        bstTest.add(root, 1);
        bstTest.add(root, 100);
        bstTest.add(root, 0);

        assertEquals("BinarySearchTreeTest.testAdd", 0, root.getLeftChild().getLeftChild().getElement());
    }

    public static void testRemoveLeaf() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(12);

        bstTest.add(root, 5);
        bstTest.add(root, 14);
        bstTest.add(root, 3);
        bstTest.add(root, 7);
        bstTest.add(root, 1);
        bstTest.add(root, 9);
        bstTest.add(root, 8);
        bstTest.add(root, 11);
        bstTest.add(root, 13);
        bstTest.add(root, 17);
        bstTest.add(root, 20);
        bstTest.add(root, 18);

        assertEquals("BinarySearchTreeTest.testRemoveLeaf", 13, bstTest.remove(root.getRightChild().getLeftChild()));
    }

    public static void testRemoveOneChildNode() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(12);

        bstTest.add(root, 5);
        bstTest.add(root, 14);
        bstTest.add(root, 3);
        bstTest.add(root, 7);
        bstTest.add(root, 1);
        bstTest.add(root, 9);
        bstTest.add(root, 8);
        bstTest.add(root, 11);
        bstTest.add(root, 13);
        bstTest.add(root, 17);
        bstTest.add(root, 20);
        bstTest.add(root, 18);

        assertEquals("BinarySearchTreeTest.testRemoveOneChildNode", 3, bstTest.remove(root.getLeftChild().getLeftChild()));
    }

    public static void testRemoveTwoChildrenNode() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(12);

        bstTest.add(root, 5);
        bstTest.add(root, 14);
        bstTest.add(root, 3);
        bstTest.add(root, 7);
        bstTest.add(root, 1);
        bstTest.add(root, 9);
        bstTest.add(root, 8);
        bstTest.add(root, 11);
        bstTest.add(root, 13);
        bstTest.add(root, 17);
        bstTest.add(root, 20);
        bstTest.add(root, 18);

        assertEquals("BinarySearchTreeTest.testRemoveTwoChildrenNode", 14, bstTest.remove(root.getRightChild()));
    }

    public static void testRemoveRoot() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(12);

        bstTest.add(root, 5);
        bstTest.add(root, 14);
        bstTest.add(root, 3);

        assertEquals("BinarySearchTreeTest.testRemoveRoot", 12, bstTest.remove(root));
    }

    public static void testToString() {
        BinarySearchTree<Integer> bstTest = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer value1, Integer value2) {
                return value1 - value2;
            }
        });

        NodeImpl<Integer> root = (NodeImpl<Integer>) bstTest.addRoot(12);

        bstTest.add(root, 5);
        bstTest.add(root, 8);
        bstTest.add(root, 14);
        bstTest.add(root, 3);

        assertObject("BinarySearchTreeTest.testToString", new String("(12(5(3)(8))(14))"), bstTest.toString());
    }
}
