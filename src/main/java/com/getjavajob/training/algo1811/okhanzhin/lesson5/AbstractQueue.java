package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.Spliterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

abstract class AbstractQueue<V> implements Queue<V> {
    @Override
    public boolean removeIf(Predicate<? super V> filter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Spliterator<V> spliterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Stream<V> stream() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Stream<V> parallelStream() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<V> iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean offer(V v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public V poll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public V element() {
        throw new UnsupportedOperationException();
    }

    @Override
    public V peek() {
        throw new UnsupportedOperationException();
    }
}
