package com.getjavajob.training.algo1811.okhanzhin.lesson6;

import util.StopWatch;

import java.util.HashMap;
import java.util.Map;

public class AssociativeArrayPerformanceTest {
    private static final int FIVE_MILLION = 5_000_000;
    private static final int FIFTEEN_MILLION = 15_000_000;
    private static final int TWENTY_FIVE_MILLION = 25_000_000;

    public static void main(String[] args) {
        testAssociativeArrayPut();
        testHashMapPut();
        testAssociativeArrayRemove();
        testHashMapRemove();
        testAssociativeArrayGet();
        testHashMapGet();
    }

    public static void testAssociativeArrayPut() {
        AssociativeArray<Integer, Integer> testArray = new AssociativeArray<>();
        StopWatch sw = new StopWatch();
        sw.start();

        for (int i = 0; i < FIVE_MILLION; i++) {
            testArray.put(i, i);
        }

        System.out.println("TestAssociativeArrayPut - add 5 000 000 element to the hash-table: " + sw.getElapsedTime() + " ms.");
    }

    public static void testHashMapPut() {
        Map<Integer, Integer> testMap = new HashMap<>();
        StopWatch sw = new StopWatch();
        sw.start();

        for (int i = 0; i < FIVE_MILLION; i++) {
            testMap.put(i, i);
        }

        System.out.println("TestHashMapPut - add 5 000 000 element to the hash-table: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAssociativeArrayRemove() {
        AssociativeArray<Integer, Integer> testArray = new AssociativeArray<>();
        StopWatch sw = new StopWatch();

        for (int i = 0; i < TWENTY_FIVE_MILLION; i++) {
            testArray.put(i, i);
        }

        sw.start();

        for (int i = 0; i < FIFTEEN_MILLION; i++) {
            testArray.remove(i);
        }

        System.out.println("TestAssociativeArrayRemove - remove 15 000 000 element from the hash-table: " + sw.getElapsedTime() + " ms.");
    }

    public static void testHashMapRemove() {
        Map<Integer, Integer> testMap = new HashMap<>();
        StopWatch sw = new StopWatch();

        for (int i = 0; i < TWENTY_FIVE_MILLION; i++) {
            testMap.put(i, i);
        }

        sw.start();

        for (int i = 0; i < FIFTEEN_MILLION; i++) {
            testMap.remove(i);
        }

        System.out.println("TestHashMapRemove - remove 15 000 000 element from the hash-table: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAssociativeArrayGet() {
        AssociativeArray<Integer, Integer> testArray = new AssociativeArray<>();
        StopWatch sw = new StopWatch();

        for (int i = 0; i < TWENTY_FIVE_MILLION; i++) {
            testArray.put(i, i);
        }

        sw.start();

        for (int i = 0; i < FIFTEEN_MILLION; i++) {
            testArray.get(i);
        }

        System.out.println("TestAssociativeArrayRemove - get 15 000 000 element from the hash-table: " + sw.getElapsedTime() + " ms.");
    }

    public static void testHashMapGet() {
        Map<Integer, Integer> testMap = new HashMap<>();
        StopWatch sw = new StopWatch();

        for (int i = 0; i < TWENTY_FIVE_MILLION; i++) {
            testMap.put(i, i);
        }

        sw.start();

        for (int i = 0; i < FIFTEEN_MILLION; i++) {
            testMap.get(i);
        }

        System.out.println("TestHashMapRemove - get 15 000 000 element from the hash-table: " + sw.getElapsedTime() + " ms.");
    }
}
