package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import java.util.TreeMap;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class SortedMapTest {
    public static void main(String[] args) {
        testSubMap();
        testHeadMap();
        testTailMap();
        testFirstKey();
        testLastKey();
        testKeySet();
        testValues();
        testEntrySet();
    }

    public static TreeMap<Integer, String> initializeMap() {
        TreeMap<Integer, String> testMap = new TreeMap<>();

        testMap.put(1, "One");
        testMap.put(2, "Two");
        testMap.put(3, "Three");
        testMap.put(4, "Four");
        testMap.put(5, "Five");
        testMap.put(6, "Six");
        testMap.put(7, "Seven");
        testMap.put(8, "Eight");
        testMap.put(9, "Nine");
        testMap.put(10, "Ten");
        testMap.put(11, "Eleven");
        testMap.put(12, "Twelve");

        return testMap;
    }

    public static void testSubMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("SortedMapTest.testSubMap", "{5=Five, 6=Six, 7=Seven, 8=Eight}", map.subMap(5, 9).toString());
    }

    public static void testHeadMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("SortedMapTest.testHeadMap", "{1=One, 2=Two}", map.headMap(3).toString());
    }

    public static void testTailMap() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("SortedMapTest.testTailMap", "{10=Ten, 11=Eleven, 12=Twelve}", map.tailMap(10).toString());
    }

    public static void testFirstKey() {
        TreeMap<Integer, String> map = initializeMap();

        assertEquals("SortedMapTest.testFirstKey", 1, map.firstKey());
    }

    public static void testLastKey() {
        TreeMap<Integer, String> map = initializeMap();

        assertEquals("SortedMapTest.testLastKey", 12, map.lastKey());
    }

    public static void testKeySet() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("SortedMapTest.testKeySet", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]", map.keySet().toString());
    }

    public static void testValues() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("SortedMapTest.testValues", "[One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Eleven, Twelve]", map.values().toString());
    }

    public static void testEntrySet() {
        TreeMap<Integer, String> map = initializeMap();

        assertObject("SortedMapTest.testValues", "[1=One, 2=Two, 3=Three, 4=Four, 5=Five, 6=Six, 7=Seven, 8=Eight, 9=Nine, 10=Ten, 11=Eleven, 12=Twelve]", map.entrySet().toString());
    }
}
