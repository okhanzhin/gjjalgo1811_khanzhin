package com.getjavajob.training.algo1811.okhanzhin.lesson7.binary;

import com.getjavajob.training.algo1811.okhanzhin.lesson7.Node;
import com.getjavajob.training.algo1811.okhanzhin.lesson7.binary.LinkedBinaryTree.NodeImpl;

import java.util.ArrayList;
import java.util.List;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;
import static util.Assert.assertNull;
import static util.Assert.assertBoolean;

public class LinkedBinaryTreeTest {
    private static final String ILLEGAL_ARGUMENT_DESCRIPTION = "IllegalArgumentException";

    public static void main(String[] args) {
        testAddRoot();
        testValidate();
        testAdd();
        testAddLeft();
        testAddRight();
        testSetNewValue();
        testSetReturnedValue();
        testRemove();
        testLeft();
        testRight();
        testInOrder();
        testPreOrder();
        testPostOrder();
        testBreadthFirst();
        testRoot();
        testParent();
        testChildren();
        testChildrenNumber();
        testSibling();
        testSiblingNullValue();
        testIsRoot();
        testIsInternal();
        testIsInternalNullValue();
        testIsExternal();
        testIsExternalNullValue();
    }

    public static void testAddRoot() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);

        assertEquals("LinkedBinaryTreeTest.testAddRoot", 1, root.getElement());
    }

    public static void testValidate() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);

        assertEquals("LinkedBinaryTreeTest.testValidate", 1, testTree.validate(testTree.root()).getElement());
    }

    public static void testAdd() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addRight(root, 2);

        assertEquals("LinkedBinaryTreeTest.testAddRoot", 2, root.getRightChild().getElement());
    }

    public static void testAddLeft() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 2);

        assertEquals("LinkedBinaryTreeTest.testAddLeft", 2, root.getLeftChild().getElement());
    }

    public static void testAddRight() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addRight(root, 3);

        assertEquals("LinkedBinaryTreeTest.testAddLeft", 3, root.getRightChild().getElement());
    }

    public static void testSetNewValue() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 2);
        testTree.set(root.getLeftChild(), 10);

        assertEquals("LinkedBinaryTreeTest.testSetNewValue", 10, root.getLeftChild().getElement());
    }

    public static void testSetReturnedValue() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 2);

        assertEquals("LinkedBinaryTreeTest.testSetReturnedValue", 2, testTree.set(root.getLeftChild(), 10));
    }

    public static void testRemove() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(0);
        testTree.addLeft(root, 1);
        testTree.addRight(root, 2);
        testTree.addLeft(root.getLeftChild(), 10);
        testTree.addRight(root.getLeftChild(), 20);
        testTree.addLeft(root.getRightChild(), 30);
        testTree.addRight(root.getRightChild(), 40);

        assertEquals("LinkedBinaryTreeTest.testRemove", 40, testTree.remove(root.getRightChild().getRightChild()));
    }

    public static void testLeft() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(0);
        testTree.addLeft(root, 100);

        assertObject("LinkedBinaryTreeTest.testLeft", root.getLeftChild(), testTree.left(root));
    }

    public static void testRight() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(0);
        testTree.addRight(root, 100);

        assertObject("LinkedBinaryTreeTest.testRight", root.getRightChild(), testTree.right(root));
    }

    public static void testInOrder() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        int[] expectedArray = new int[]{111, 11, 112, 1, 121, 12, 122};
        List<Integer> actualArray = new ArrayList<>();

        for (Node<Integer> node : testTree.inOrder()) {
            actualArray.add(node.getElement());
        }

        assertEquals("LinkedBinaryTreeTest.testInOrder", expectedArray, actualArray.toArray());
    }

    public static void testPreOrder() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        int[] expectedArray = new int[]{1, 11, 111, 112, 12, 121, 122};
        List<Integer> actualArray = new ArrayList<>();

        for (Node<Integer> node : testTree.preOrder()) {
            actualArray.add(node.getElement());
        }

        assertEquals("LinkedBinaryTreeTest.testPreOrder", expectedArray, actualArray.toArray());
    }

    public static void testPostOrder() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        int[] expectedArray = new int[]{111, 112, 11, 121, 122, 12, 1};
        List<Integer> actualArray = new ArrayList<>();

        for (Node<Integer> node : testTree.postOrder()) {
            actualArray.add(node.getElement());
        }

        assertEquals("LinkedBinaryTreeTest.testPostOrder", expectedArray, actualArray.toArray());
    }

    public static void testBreadthFirst() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        int[] expectedArray = new int[]{1, 11, 12, 111, 112, 121, 122};
        List<Integer> actualArray = new ArrayList<>();

        for (Node<Integer> node : testTree.breadthFirst()) {
            actualArray.add(node.getElement());
        }

        assertEquals("LinkedBinaryTreeTest.testBreadthFirst", expectedArray, actualArray.toArray());
    }

    public static void testRoot() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);

        assertEquals("LinkedBinaryTreeTest.testRoot", 1, testTree.root().getElement());
    }

    public static void testParent() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 100);

        assertEquals("LinkedBinaryTreeTest.testParent", 1, testTree.parent(root.getLeftChild()).getElement());
    }

    public static void testChildren() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 100);
        testTree.addRight(root, 200);

        int[] expectedChildren = new int[]{100, 200};
        List<Integer> actualArray = new ArrayList<>();

        for (Node<Integer> node : testTree.children(root)) {
            actualArray.add(node.getElement());
        }

        assertEquals("LinkedBinaryTreeTest.testChildren", expectedChildren, actualArray.toArray());
    }

    public static void testChildrenNumber() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 100);
        testTree.addRight(root, 200);

        assertEquals("LinkedBinaryTreeTest.testChildrenNumber", 2, testTree.childrenNumber(root));
    }

    public static void testSibling() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 100);
        testTree.addRight(root, 200);

        assertEquals("LinkedBinaryTreeTest.testSibling", 200, testTree.sibling(root.getLeftChild()).getElement());
    }

    public static void testSiblingNullValue() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 100);

        assertNull("LinkedBinaryTreeTest.testSiblingNullValue", null, testTree.sibling(root.getLeftChild()));
    }

    public static void testIsRoot() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);

        assertBoolean("LinkedBinaryTreeTest.testIsRoot", true, testTree.isRoot(testTree.root()));
    }

    public static void testIsInternal() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        assertBoolean("LinkedBinaryTreeTest.testIsInternal", true, testTree.isInternal(root.getRightChild()));
    }

    public static void testIsInternalNullValue() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        try {
            testTree.isInternal(root.getLeftChild().getRightChild().getLeftChild());
        } catch (IllegalArgumentException e) {
            assertObject("LinkedBinaryTreeTest.testIsInternalNullValue", ILLEGAL_ARGUMENT_DESCRIPTION, e.getMessage());
        }
    }

    public static void testIsExternal() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        assertBoolean("LinkedBinaryTreeTest.testIsExternal", true, testTree.isExternal(root.getRightChild().getRightChild()));
    }

    public static void testIsExternalNullValue() {
        LinkedBinaryTree<Integer> testTree = new LinkedBinaryTree<>();
        NodeImpl<Integer> root = (NodeImpl<Integer>) testTree.addRoot(1);
        testTree.addLeft(root, 11);
        testTree.addRight(root, 12);
        testTree.addLeft(root.getLeftChild(), 111);
        testTree.addRight(root.getLeftChild(), 112);
        testTree.addLeft(root.getRightChild(), 121);
        testTree.addRight(root.getRightChild(), 122);

        try {
            testTree.isExternal(root.getLeftChild().getRightChild().getLeftChild());
        } catch (IllegalArgumentException e) {
            assertObject("LinkedBinaryTreeTest.testIsExternalNullValue", ILLEGAL_ARGUMENT_DESCRIPTION, e.getMessage());
        }
    }
}
