package com.getjavajob.training.algo1811.okhanzhin.lesson3;

import util.StopWatch;
import java.util.ArrayList;
import java.util.List;

public class DynamicArrayPerformanceTest {
    private static final int ONE_MILLION_ITERATION = 1_000_000;
    private static final int TEN_THOUSANDS = 10_000;
    private static final int FIVE_HUNDRED_THOUSANDS = 500_000;
    private static final int TWENTY_FIVE_THOUSAND = 25_000;

    public static void main(String[] args) {
        testBooleanAddDynamicArray();
        testBooleanAddArrayList();
        System.out.println("\n\n");

        testAddFirstDynamicArray();
        testAddFirstArrayList();
        System.out.println("\n\n");

        testAddMiddleDynamicArray();
        testAddMiddleArrayList();
        System.out.println("\n\n");

        testAddLastDynamicArray();
        testAddLastArrayList();
        System.out.println("\n\n");

        testBooleanRemoveDynamicArray();
        testBooleanRemoveArrayList();
        System.out.println("\n\n");

        testRemoveFirstDynamicArray();
        testRemoveFirstArrayList();
        System.out.println("\n\n");

        testRemoveMiddleDynamicArray();
        testRemoveMiddleArrayList();
        System.out.println("\n\n");

        testRemoveLastDynamicArray();
        testRemoveLastArrayList();
        System.out.println("\n\n");
    }

    public static void initStartDynamicArray(DynamicArray<Integer> arrayList) {
        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            arrayList.add(i);
        }
    }

    public static void initStartArrayList(List<Integer> arrayList) {
        for (int i = 0; i < ONE_MILLION_ITERATION; i++) {
            arrayList.add(i);
        }
    }

    public static void testBooleanAddDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 60; i++) {
            dynamicArray.add(i);
        }

        System.out.println("TestBooleanAddDynamicArray - add 60 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testBooleanAddArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 60; i++) {
            arrayList.add(i);
        }

        System.out.println("TestBooleanAddArrayList - add 60 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }


    public static void testAddFirstDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS; i++) {
            dynamicArray.add(0, i);
        }

        System.out.println("TestAddFirstDynamicArray - add 10 000 element to the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddFirstArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS; i++) {
            arrayList.add(0, i);
        }

        System.out.println("TestAddFirstArrayList - add 10 000 element to the top of the list: " + sw.getElapsedTime() + " ms.");
    }


    public static void testAddMiddleDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            dynamicArray.add(FIVE_HUNDRED_THOUSANDS, i);
        }

        System.out.println("TestAddMiddleDynamicArray - add 500 000 element to the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddMiddleArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            arrayList.add(FIVE_HUNDRED_THOUSANDS, i);
        }

        System.out.println("TestAddMiddleArrayList - add 500 000 element to the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddLastDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 60; i++) {
            dynamicArray.add(dynamicArray.size(), i);
        }

        System.out.println("TestAddLastDynamicArray - add 500 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testAddLastArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < ONE_MILLION_ITERATION * 60; i++) {
            arrayList.add(arrayList.size(), i);
        }

        System.out.println("TestAddLastArrayList - add 1 000 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }


    public static void testBooleanRemoveDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS; i++) {
            dynamicArray.remove(dynamicArray.get(i));
        }

        System.out.println("TestBooleanAddDynamicArray - add 10 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testBooleanRemoveArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS; i++) {
            arrayList.remove(arrayList.get(i));
        }

        System.out.println("TestBooleanRemoveArrayList - add 10 000 element to the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveFirstDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS; i++) {
            dynamicArray.remove(i);
        }

        System.out.println("TestRemoveFirstDynamicArray - remove 10 000 element from the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveFirstArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < TEN_THOUSANDS; i++) {
            arrayList.remove(i);
        }

        System.out.println("TestRemoveFirstArrayList - remove 10 000 element from the top of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveMiddleDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            dynamicArray.remove(FIVE_HUNDRED_THOUSANDS);
        }

        System.out.println("TestRemoveMiddleDynamicArray - remove 25 000 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveMiddleArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND; i++) {
            arrayList.remove(FIVE_HUNDRED_THOUSANDS);
        }

        System.out.println("TestRemoveMiddleArrayList - remove 25 000 element from the middle of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveLastDynamicArray() {
        StopWatch sw = new StopWatch();
        DynamicArray<Integer> dynamicArray = new DynamicArray<>();

        initStartDynamicArray(dynamicArray);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND * 7; i++) {
            dynamicArray.remove(dynamicArray.size() - 1 - i);
        }

        System.out.println("TestRemoveLastDynamicArray - remove 175 000 element from the end of the list: " + sw.getElapsedTime() + " ms.");
    }

    public static void testRemoveLastArrayList() {
        StopWatch sw = new StopWatch();
        List<Integer> arrayList = new ArrayList<>();

        initStartArrayList(arrayList);

        sw.start();

        for (int i = 0; i < TWENTY_FIVE_THOUSAND * 7; i++) {
            arrayList.remove(arrayList.size() - 1 - i);
        }

        System.out.println("TestRemoveLastArrayList - remove 175 000 element from the end of the list: " + sw.getElapsedTime() + " ms.");
    }
}
