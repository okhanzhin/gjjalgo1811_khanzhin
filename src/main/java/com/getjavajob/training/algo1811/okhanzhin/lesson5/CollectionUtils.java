package com.getjavajob.training.algo1811.okhanzhin.lesson5;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@FunctionalInterface
interface Filter<V> {
    boolean checkElement(V element);
}

@FunctionalInterface
interface Transformer<I, O> {
    O changeType(I element);
}

@FunctionalInterface
interface Closure<V> {
    void execute(V element);
}

public class CollectionUtils {
    public static <V> boolean filter(List<V> list, Filter<V> filter) {
        List<V> inputList = new ArrayList<>();
        Iterator<V> iterator = list.iterator();

        while (iterator.hasNext()) {
            V element = iterator.next();
            if (filter.checkElement(element)) {
                iterator.remove();
            }
        }

        return !inputList.equals(list);
    }

    public static <I, O> void transform(List<I> list, Transformer<I, O> transformer) {
        ListIterator iterator = list.listIterator();

        while (iterator.hasNext()) {
            iterator.set(transformer.changeType((I) iterator.next()));
        }
    }

    public static <I, O> List<O> transformIntoNewCollection(List<I> list, Transformer<I, O> transformer) {
        List<O> resultList = new ArrayList<>();
        ListIterator<I> iterator = list.listIterator();

        while (iterator.hasNext()) {
            resultList.add(transformer.changeType((iterator.next())));
        }

        return resultList;
    }

    public static <V> List<V> forAllDo(List<V> list, Closure<V> closure) {

        for (V element : list) {
            closure.execute(element);
        }

        return list;
    }

    public static <V> Collection<V> unmodifiableCollection(List<V> list) {
        return new UnmodifiableCollection<>(list);
    }

    public static class UnmodifiableCollection<V> extends AbstractCollection<V> implements Collection<V> {
        Collection<V> collection;

        public UnmodifiableCollection(Collection<V> collection) {
            this.collection = collection;
        }

        @Override
        public int size() {
            return this.collection.size();
        }

        @Override
        public boolean isEmpty() {
            return this.collection.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return this.collection.contains(o);
        }

        @Override
        public Object[] toArray() {
            return this.collection.toArray();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return this.collection.toArray(a);
        }

        @Override
        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends V> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }

        @Override
        public String toString() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<V> iterator() {
            return null;
        }
    }
}
