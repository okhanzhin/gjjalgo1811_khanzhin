package com.getjavajob.training.algo1811.okhanzhin.lesson9;

import java.util.NavigableSet;
import java.util.TreeSet;

import static util.Assert.assertEquals;
import static util.Assert.assertObject;

public class NavigableSetTest {
    public static void main(String[] args) {
        testLower();
        testFloor();
        testCeiling();
        testHigher();
        testPollFirst();
        testPollLast();
        testDescendingSet();
        testSubSet();
        testHeadSet();
        testTailSet();
    }

    public static NavigableSet<Integer> initializeSet() {
        NavigableSet<Integer> testSet = new TreeSet<>();

        testSet.add(1);
        testSet.add(2);
        testSet.add(3);
        testSet.add(4);
        testSet.add(5);
        testSet.add(6);
        testSet.add(7);
        testSet.add(8);
        testSet.add(9);
        testSet.add(10);

        return testSet;
    }

    public static void testLower() {
        NavigableSet<Integer> set = initializeSet();

        assertEquals("NavigableSetTest.testLower", 7, set.lower(8));
    }

    public static void testFloor() {
        NavigableSet<Integer> set = initializeSet();

        assertEquals("NavigableSetTest.testFloor", 8, set.floor(8));
    }

    public static void testCeiling() {
        NavigableSet<Integer> set = initializeSet();

        assertEquals("NavigableSetTest.testCeiling", 8, set.ceiling(8));
    }

    public static void testHigher() {
        NavigableSet<Integer> set = initializeSet();

        assertEquals("NavigableSetTest.testHigher", 9, set.higher(8));
    }

    public static void testPollFirst() {
        NavigableSet<Integer> set = initializeSet();

        assertEquals("NavigableSetTest.testPollFirst", 1, set.pollFirst());
    }

    public static void testPollLast() {
        NavigableSet<Integer> set = initializeSet();

        assertEquals("NavigableSetTest.testPollLast", 10, set.pollLast());
    }

    public static void testDescendingSet() {
        NavigableSet<Integer> set = initializeSet();

        assertObject("NavigableSetTest.testDescendingSet", "[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]",
                                set.descendingSet().toString());
    }

    public static void testSubSet() {
        NavigableSet<Integer> set = initializeSet();

        assertObject("NavigableSet.testSubSet", "[3, 4, 5]", set.subSet(3, true, 5, true).toString());
    }

    public static void testHeadSet() {
        NavigableSet<Integer> set = initializeSet();

        assertObject("NavigableSet.testHeadSet", "[1, 2, 3]", set.headSet(3, true).toString());
    }

    public static void testTailSet() {
        NavigableSet<Integer> set = initializeSet();

        assertObject("NavigableSet.testTailSet", "[9, 10]", set.tailSet(8, false).toString());
    }
}
