package com.getjavajob.training.algo1811.okhanzhin.lesson10;

public class BubbleSort {
    public static int[] sort(int[] array) {
        int temp;
        boolean swapped;
        for (int i = 0; i < array.length - 1; i++) {
            swapped = false;
            for (int j = 1; j < array.length - i; j++) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                    swapped = true;
                }
            }
            if (!swapped) {
                System.out.println("Already Swapped!");
                break;
            }
        }
        return array;
    }
}
