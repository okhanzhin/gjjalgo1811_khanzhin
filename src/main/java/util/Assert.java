package util;

import java.util.Arrays;

public class Assert {

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertStringBuilder(String testName, StringBuilder expected, StringBuilder actual) {
        if (expected.toString().equals(actual.toString())) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \n" + Arrays.toString(expected) + "\n actual \n" +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, String[] expected, String[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \n" + Arrays.toString(expected) + "\n actual \n" +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[] expected, Object[] actual) {
        if (Arrays.toString(expected).equals(Arrays.toString(actual))) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \n" + Arrays.toString(expected) + "\n actual \n" +
                    Arrays.toString(actual));
        }
    }

    public static void assertObject(String testName, Object expected, Object actual) {
        if (actual.equals(expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(Object expected, Object actual) {
        if (expected.equals(actual)) {
            System.out.println("Test throwing Exception passed.");
        } else {
            System.out.println("Failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertBoolean(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertNull(String testName, Object expected, Object actual) {
        if (actual == expected) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void fail(String description) {
        throw new AssertionError(description);
    }
}
